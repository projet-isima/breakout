import os
from typing import Dict, Union

import pygame

from constantes import *
from GamePhase import GamePhase
from State import State
from Item import Item
from ShopScene import ShopScene 
from abc import ABC,abstractmethod

class AbstractLevelLoader(State,ABC):
    def __init__(self, game, levelname: str, shopAvailable = True) -> None:
        State.__init__(self,game)
        self.levelname = levelname
        self.screen = pygame.display.get_surface()
        self.scenes = sorted(os.listdir(os.path.join("Levels", levelname)))
        self.selected_index = 0
        self.actualSceneScore = 0
        self.previousScore = 0
        self.actualScore = 0
        self.perks = self._init_perks()
        self.shopAvailable = shopAvailable

        self.actualScene = self.loadNextScene()

        self.star_indicator = Item(5, DISPLAY_WIDTH-100, 10)
        
        self.previous_number_of_stars = 0
        self.actuel_scene_number_of_stars = 0
        self.actual_number_of_stars = 0



    @abstractmethod
    def _init_perks(self):
        """initialise les avantages du joueur"""
        ...
        
    @abstractmethod
    def _load_next_game_scene(self):
        """charge la scène de jeu suivante"""
        ...
    
    def loadNextScene(self,shop=False) -> Union[GamePhase, None]:
        if (self.noMoreScene()):
            return None
        if not shop:
            self._load_next_game_scene()
        else:    
            self.actualScene = ShopScene(self)
            
        return self.actualScene

    def noMoreScene(self) -> bool:
        return self.selected_index >= len(self.scenes)

    def actualSceneStatus(self) -> Union[GamePhase, None]:
        return self.actualScene.getStatus() if self.actualScene else None


    def _update_keystate(self,keystate):
        """Altère le dictionnaire des clés"""
        pass
    
    def _manage_ongoing_scene(self, key_state: Dict[str, KeyData]) -> None:
        if self.actualScene:
            self._update_keystate(key_state)
            self.actualScene.update(key_state)
            if self._isGamePhase(self.actualScene):
                self.actualSceneScore = self.actualScene.getScore()
                self.actualScore = self.previousScore + self.actualSceneScore
                
            self.actuel_scene_number_of_stars = self.actualScene.get_number_of_stars()
            self.actual_number_of_stars =  self.previous_number_of_stars + self.actuel_scene_number_of_stars

    def _manage_lost_scene(self) -> None:
        if self.actualScene:
            self.actualSceneScore = self.actualScene.getScore()
            self.actualScore = self.previousScore + self.actualSceneScore
            self.game.popStateStack()

    def _manage_won_scene(self) -> None:
        if (self.noMoreScene()):
            self.game.popStateStack()
        else:
            if self._isGamePhase(self.actualScene):
                self.previousScore = self.actualSceneScore + self.previousScore
                self.actualScore = self.previousScore
            
            self.previous_number_of_stars +=  self.actualScene.get_number_of_stars()
            
            self.actual_number_of_stars = self.previous_number_of_stars
            self.actualSceneScore = 0
            shopping = self._isGamePhase(self.actualScene) if self.shopAvailable else False
            self.loadNextScene(shop=shopping)

    def _render_star_indicator(self) -> None:
        self.star_indicator.draw()
        
        self.game.draw_text(f"X {self.actual_number_of_stars}",
                            (DISPLAY_WIDTH-50, 10), (255, 255, 255))
        
    def _render_life_indicator(self) -> None:
        self.game.draw_text(f"Vies : {self.actualScene.get_number_of_lives()}",(0,10),(255,255,255))
        
    @abstractmethod
    def _isGamePhase(self,scene):
        """Vérifie si la scène du jeu actuelle est une scène de jeu"""
        ...

    def update(self, key_state: Dict[str, KeyData]) -> None:
        if self.actualScene is None:
            self.game.popStateStack()

        else:
            if self.actualSceneStatus() == SCENE_ONGOING:
                self._manage_ongoing_scene(key_state)

            elif self.actualSceneStatus() == SCENE_DEFEAT:
                self._manage_lost_scene()

            elif self.actualSceneStatus() == SCENE_VICTORY:
                self._manage_won_scene()

            self.actualScene.update(key_state)
            if self._isGamePhase(self.actualScene):
                self.actualSceneScore = self.actualScene.getScore()
                
            self.actual_number_of_stars = self.previous_number_of_stars + self.actualScene.get_number_of_stars()


    def render(self) -> None:
        if self.actualScene is not None:
            self.actualScene.render()

        self.game.draw_text(f"Score:{self.actualScore}", (
            DISPLAY_WIDTH-TILE_HEIGHT, DISPLAY_HEIGHT//2 - 50), (255, 255, 255), rot=90)
        
        self._render_star_indicator()
        
        if self._isGamePhase(self.actualScene):
            self._render_life_indicator()