import itertools
import time
import math
import pygame
from Barre import Barre
from BalleMulti import BalleMulti
from Tile import Tile
from Pause import Pause
from constantes import *
import pickle
from Particle import Particle
from Item import Item
import random
from pygame import mixer
from GamePhase import GamePhase

class GamePhaseMulti(GamePhase):

    def __init__(self, game, tiles=None, lives = 3, allow_items=True,player_perk=None):
        super().__init__(game,tiles,lives,allow_items,player_perk)
       
       
    def _init_entities(self,player_perk=None):
        
        self._barre1 = Barre(self, self.display_width//4 -
                            60, self.display_height - 40)
        self._barre2 = Barre(self, self.display_width//4*3 -
                            60, self.display_height - 40)
        self._barres = [self._barre1, self._barre2]
        
        if player_perk != None:
            for i in range(2):
                if (player_perk[i] != 0):
                    self._barres[i].receiveSignal(player_perk[i])

        self._ball1 = BalleMulti(self._barre1.rect.x + self._barre1.width//2 - 7,
                           self.display_height - TILE_WIDTH - 4, self._barre1, self._barre2, self._tiles)

        self._ball2 = BalleMulti(self._barre2.rect.x + self._barre2.width//2 - 7,
                           self.display_height - TILE_WIDTH - 4, self._barre1, self._barre2, self._tiles)

        self._balls = [self._ball1, self._ball2]
        
        
        
    def getStatus(self):
        return self._status
    
    def get_number_of_stars(self):
        return self.number_of_stars_collected
    
    def get_number_of_lives(self):
        return self.number_of_lives


    @classmethod
    def constructFromTiles(cls, game, tiles,player_perk=None):
        return GamePhaseMulti(game, tiles,player_perk=player_perk)
    
    def _process_horizontal_movement(self, key_state):
        
        
        if key_state["right"][0] == True:
            self._barres[1].move("right")
            
        elif key_state["left"][0] == True:
            self._barres[1].move("left")
        
        else:
            self._barres[1].move("")

        if key_state["q"][0] == True:
            self._barres[0].move("left")
            
        elif key_state["d"][0] == True:
            self._barres[0].move("right")
            
        else:
            self._barres[0].move("")
          
    def _process_pre_launch_ball_orientation(self, key_state):

       
        if key_state["up"][0] == True:
            self._balls[1].choose_ball_orientation("up")

        if key_state["down"][0] == True:
            self._balls[1].choose_ball_orientation("down")
            
        if key_state["z"][0] == True:
            self._balls[0].choose_ball_orientation("up")

        if key_state["s"][0] == True:
            self._balls[0].choose_ball_orientation("down")


        if key_state["space"][0] == True:
            self._prelaunch_on_space_pressed()
                      
        self._balls[0].rect.x = self._barre1.rect.x + \
            self._barre1.rect.w // 2 - self._balls[0].rect.w // 2

        self._balls[1].rect.x = self._barre2.rect.x + \
            self._barre2.rect.w // 2 - self._balls[1].rect.w // 2

    
 
    def _check_item_collision(self, item):
        return next((barre for barre in self._barres if barre.isInCollisionWith(item)), None) 

    def addABall(self):

        ball = BalleMulti(self._balls[0].rect.x, self._balls[0].rect.y,
                      self._barre1, self._barre2, self._tiles, (random.random(),random.random()), True)
        self._balls.append(ball)

    
    def spawnParticlesAt(self, pos: Position,color=(255,255,255)):
        for _ in range(math.floor(self._balls[0].getSpeed() * 5)):
            self._particle_array.append(Particle(2, *pos,color))

    def _update_players(self):
        if self._barres[0].rect.right > self._barres[1].rect.left:
            
            self._barre1.move("")
            self._barre2.move("")
            while self._barres[0].rect.right > self._barres[1].rect.left:
                self._barres[0].rect.x -= 1
                self._barres[1].rect.x += 1
            
        
        for barre in self._barres:
            barre.update()
            
        
                 
    def _draw_players(self):
        for barre in self._barres:
            barre.draw()
        
    def drawEntities(self):

        super().drawEntities()

        self.game.draw_text("1", (
            self._barre1.rect.x + self._barre1.width/2, self._barre1.rect.y - 20), (255, 0, 0))
        self.game.draw_text("2", (
            self._barre2.rect.x + self._barre2.width/2, self._barre2.rect.y - 20), (0, 0, 255))

        
                
    

   