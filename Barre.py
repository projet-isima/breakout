import pygame as pg
import os
from constantes import *
from Projectile import Projectile

class Barre(pg.sprite.Sprite):


    def __init__(self,scene,x,y):
        super().__init__()
        
        self.screen = pg.display.get_surface()
        
        self.scene = scene
        
        self.player_img_dir = os.path.join("assets","Player")
        self.display_width = self.screen.get_size()[0]

        self.active_upgrade = 0
        self.normal_player_sprites = []
        
        self.width = 2 * TILE_WIDTH
        self.height = TILE_HEIGHT
        
        
        self.inplay = False
        
        self.expanded_sprites = []
        
        for i in range(1,4):
            img = pg.image.load(os.path.join(self.player_img_dir,f"NormalAnim{i}.png"))
            img = pg.transform.scale(img,(self.width,self.height))
            self.normal_player_sprites.append(img)
        
        for i in range(1,4):
            img = pg.image.load(os.path.join(self.player_img_dir,f"nlongAnim{i}.png"))
            img = pg.transform.scale(img,(783 * self.height//128,self.height))
            self.expanded_sprites.append(img)
        
        short_sprite = pg.image.load(os.path.join(self.player_img_dir,"Short.png"))
        short_sprite = pg.transform.scale(short_sprite,(self.width//2,self.height))
        self.short_sprites = [short_sprite]
        
        
        
        self.laser_sprites = []
        for i in range(1,4):
            img = pg.image.load(os.path.join(self.player_img_dir,f"LaserAnim{i}.png"))
            img = pg.transform.scale(img,(self.width,self.height))
            self.laser_sprites.append(img)
            
        
        self.sprites = self.normal_player_sprites
        self.current_sprite = 0
        
        self.img = self.expanded_sprites[0]
        self.rect = self.img.get_rect(topleft=(x,y))

        self.vitesseMax = 10
        self.vitesse = 0
        self.dir = ""

        self.acceleration = 0.5
        self.freinage = 0.5

        self.shooting = False
        self.last_shoot = 0
        self.delay_shoot = 600

    def _manage_speed(self):
        """Change la vitesse selon la direction actuelle de la barre"""
        if self.dir == "left":
            self.vitesse -= self.freinage if self.vitesse > 0 else self.acceleration
            if (self.vitesse < -1 * self.vitesseMax):
                self.vitesse = -1*self.vitesseMax
            elif (self.vitesse * (self.vitesse + self.acceleration)  < 0):
                self.vitesse = 0

        elif self.dir == "right":
            self.vitesse += self.freinage if self.vitesse < 0 else self.acceleration
            if (self.vitesse > self.vitesseMax):
                self.vitesse = self.vitesseMax
            elif (self.vitesse * (self.vitesse - self.acceleration)  < 0):
                self.vitesse = 0
        else:
            self.vitesse = 0

        self.rect.x += self.vitesse
        if self.rect.right > self.display_width :
            self.rect.right = self.display_width
            self.dir = ""
            self.vitesse = 0
        if self.rect.left < 0:
            self.rect.left = 0
            self.dir = ""
            self.vitesse = 0



    def update(self):
        self.current_sprite += 0.1

        if self.current_sprite >= len(self.sprites):
            self.current_sprite = 0
        self.img = self.sprites[int(self.current_sprite)]
        self.rect = self.img.get_rect(topleft=(self.rect.x,self.rect.y))

        
        self._manage_speed()
        
        if self.shooting == True and (
            pg.time.get_ticks() - self.last_shoot > self.delay_shoot
        ):
            LASER_SOUND.play()
            self.last_shoot = pg.time.get_ticks()
            self.shootLeft()
            self.shootRight()
            
            
    
    def get_position(self):
        return self.rect.x, self.rect.y
        
    def shootLeft(self):
        if self.inplay:
            self.scene.lasers.add(Projectile(self.rect.left,self.rect.top,8))
    
    def shootRight(self):
        if self.inplay:
            self.scene.lasers.add(Projectile(self.rect.right,self.rect.top,8))
        
    def get_size(self):
        return self.rect.w,self.rect.h
    
    def get_active_upgrade(self):
        return self.active_upgrade
    
    def isInCollisionWith(self,sprite):
        return pg.sprite.collide_rect(self,sprite)
            
    
    def receiveSignal(self,signal):
        """Reçoit le type d'un item et réagit en conséquence """
        if signal == 1:
            self.shooting = False
            if self.sprites == self.short_sprites:
                self.sprites = self.normal_player_sprites
                self.width *= 2
                self.active_upgrade = 0
            elif self.sprites == self.normal_player_sprites:
                self.sprites = self.expanded_sprites
                self.width *= 2
                self.active_upgrade = 1
            else:
                self.sprites = self.expanded_sprites
                
        elif signal == 2:
            self.shooting = False
            if self.sprites == self.expanded_sprites:
                self.sprites = self.normal_player_sprites
                self.width /= 2
                self.active_upgrade = 0
            elif self.sprites == self.normal_player_sprites :
                self.sprites = self.short_sprites
                self.width /= 2
                self.active_upgrade = 2
            else:
                self.sprites = self.short_sprites

        elif signal == 4:
            self.sprites = self.laser_sprites
        
            self.active_upgrade = 4
            self.shooting = True
        self.rect = self.sprites[0].get_rect(topleft = (self.rect.left,self.rect.top))
        

    def move(self, dep):

        self.dir = dep
        
        

    def draw(self):
        
        self.screen.blit(self.img, self.rect)