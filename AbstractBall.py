import pygame as pg
import random
from constantes import *
import os

from abc import ABC,abstractmethod

class AbstractBall(pg.sprite.Sprite, ABC):

    def __init__(self, x : float, y : float, tiles, orientation=(0, -1), launched : bool = False) -> None:
        ABC().__init__()
        pg.sprite.Sprite.__init__(self)
        self.screen = pg.display.get_surface()

        img_file = os.path.join("assets", "Ball", "balle.png")
        self.img = pg.image.load(img_file).convert_alpha()
        self.img = pg.transform.scale(self.img, (14, 14))

        self.rect = self.img.get_rect(topleft=(x, y))

        self.orientation = pg.math.Vector2(orientation)
        self.orientation.normalize()
        
        self.tiles = tiles

        self.display_width, self.display_height = self.screen.get_size()
        self.width, self.height = self.rect.width, self.rect.height

        self.speed = 4
        self.launched = launched
        self.max_speed = 8
        
    
    


    @abstractmethod
    def _init_paddles(self,data):
        """
            Initialise les raquêtes
        """
        ...

    def getSpeed(self) -> float:

        return self.speed

    def launch(self) -> None:
        self.launched = True

    def choose_ball_orientation(self, dep : str): 
        
        """Déplace l'orientation de la balle dans le sens spécifée par dep: up -> vers la droite de la 
        verticale vers le haut; down -> vers la gauche de la verticale vers le haut;
        """
        
        current_angle = math.radians(
            self.orientation.angle_to(pg.Vector2(1, 0)))

        if dep == "down":
            current_angle -= math.pi/100

            if (current_angle > math.pi-0.05):
                current_angle = math.pi-0.05
            current_angle = max(current_angle, 0.05)
            
        elif dep == "up":
            current_angle += math.pi/100

            if (current_angle > math.pi-0.05):
                current_angle = math.pi-0.05
            current_angle = max(current_angle, 0.05)
        self.orientation.x = math.cos(current_angle)
        self.orientation.y = -math.sin(current_angle)


    
    def get_velocity(self): return self.orientation * self.speed 
    
    def move(self,ignore=False):
        
        
        self.rect.x = round(self.rect.x + self.speed * self.orientation.x)
        self.rect.y = round(self.rect.y + self.speed * self.orientation.y)

        return self.collision(self.tiles)

    def reflect(self, normal_vector):
        """Réfléchit le vecteur orientation selon la normale spécifiée"""
        if normal_vector.magnitude() != 0:
            self.orientation = self.orientation.reflect(normal_vector)

    def get_orientation(self): return self.orientation.x,self.orientation.y
    
    def collision_mur(self):
        """Gère la collision avec les murs"""
        normal_vector = pg.math.Vector2()
        collision = False
        if self.rect.top < 3:  # haut
            normal_vector = pg.math.Vector2(0, -1)
            self.rect.top = 3
            collision = True

        if self.rect.left < 3:  # gauche
            normal_vector = pg.math.Vector2(1, 0)
            self.rect.left = 3
            collision = True

        if self.rect.right > self.display_width - 3:  # droite
            normal_vector = pg.math.Vector2(-1, 0)
            self.rect.right = self.display_width - 3
            collision = True
        
        self.reflect(normal_vector)

        return collision
    

    
    def _reflect_on_paddle(self,paddle):
        
        """Gère la collision sur la barre spécifiée""" 
        self.rect.left -= round(self.orientation.x * self.speed)
        self.rect.top -= round(self.orientation.y * self.speed)
        
        self.orientation.y *= -1
        self.orientation.x += paddle.vitesse / paddle.vitesseMax 
        
        self.orientation = pg.math.Vector2.normalize(self.orientation)

        self.rect.bottom = paddle.rect.top  
        
        if abs(self.orientation.y) < math.sin(math.pi/16):
            self.orientation.y = math.sin(math.pi/16) if self.orientation.y > 0 else -math.sin(math.pi/16)

            self.orientation = pg.math.Vector2.normalize(self.orientation)
        
        self.rect.left += round(self.orientation.x * self.speed)
        self.rect.top += round(self.orientation.y * self.speed)
            
        
    @abstractmethod
    def collision_barre(self):
        """vérifie et gère la collion avec une barre"""
        ...

    def get_pos(self):
        return (self.rect.x, self.rect.y)

    def collision_tile(self, tiles):

        """Gère la collision avec les tuiles"""
        hits = pg.sprite.spritecollide(self, tiles,False)
        collision = False

        hit = None
        if len(hits) > 0:

            collision = True
            hit = hits[0]

            offset = round(self.orientation.x * self.speed), round(self.orientation.y * self.speed)

            if (self.rect.right >= hit.rect.left or self.rect.left <= hit.rect.left) and hit.rect.top <= self.rect.top and hit.rect.bottom >= self.rect.bottom: 
                self.orientation.x *= - 1  #dans la tuile par la droite / gauche

            elif (self.rect.bottom >= hit.rect.top or self.rect.top <= hit.rect.bottom) and hit.rect.left <= self.rect.left and hit.rect.right >= self.rect.right:
                self.orientation.y *= -1 # dans la tuile par le haut / le bas
            else:
                
                
                
                if hit.rect.collidepoint(self.rect.topright):
                    if self.orientation.x > 0 and self.orientation.y < 0:
                        self.orientation.x *= -1 
                        self.orientation.y *= -1 
                    if self.orientation.y < 0 and self.orientation.x < 0:
                        self.orientation.y *= -1
                    if self.orientation.y > 0 and self.orientation.x > 0:
                        self.orientation.x *= -1

                if hit.rect.collidepoint(self.rect.topleft):
                    if self.orientation.x < 0 and self.orientation.y < 0:
                        self.orientation.x *= -1 
                        self.orientation.y *= -1 
                    if self.orientation.y < 0 and self.orientation.x > 0:
                        self.orientation.y *= -1
                    if self.orientation.y > 0 and self.orientation.x < 0:
                        self.orientation.x *= -1
                   

                if hit.rect.collidepoint(self.rect.bottomleft):
                    if self.orientation.x < 0 and self.orientation.y > 0:
                        self.orientation.x *= -1 
                        self.orientation.y *= -1 
                    if self.orientation.y > 0 and self.orientation.x > 0:
                        self.orientation.y *= -1
                    if self.orientation.y < 0 and self.orientation.x < 0:
                        self.orientation.x *= -1
                  

                if hit.rect.collidepoint(self.rect.bottomright):
                    
                    if self.orientation.x > 0 and self.orientation.y > 0:
                        self.orientation.x *= -1 
                        self.orientation.y *= -1 
                    if self.orientation.y > 0 and self.orientation.x < 0:
                        self.orientation.y *= -1
                    if self.orientation.y < 0 and self.orientation.x > 0:
                        self.orientation.x *= -1
            
            while pg.sprite.collide_rect(self,hit): 
                self.rect.left -= offset[0]
                self.rect.top -= offset[1]


        return (collision, hit)

    def up_speed(self):
        """Augmente la vitesse et borne l'angle de la direction"""
        self.speed = min(self.speed + 0.01, self.max_speed)
        if abs(self.orientation.y) < math.sin(math.pi/16):
            self.orientation.y = -math.sin(math.pi/16) if self.orientation.y < 0 else math.sin(math.pi/16)
        self.orientation = pg.math.Vector2.normalize(self.orientation)

    def isOffScreen(self):
        return self.rect.y > DISPLAY_HEIGHT

    def collision(self, tiles):
        """Gère toutes les collisions et renvoie un tuple 
        (la coliision a eu lieu?, la tuile touchée éventuellement"""
        data_coll_barre = self.collision_barre()
        if data_coll_barre[0]:
            return True,data_coll_barre[1]
        
        coll_mur = self.collision_mur()
        coll_tile = self.collision_tile(tiles)
        

        return coll_tile[0] or coll_mur, coll_tile[1] 

    def main_menu_init(self):
        """Initialisation pour l'écran de début"""
        self.launched = True
        self.orientation.x = 1
        self.orientation.y = -1

        while 162 < self.rect.x < 177 + 251 and 45 < self.rect.y < 59 + 294:
            self.rect.x = random.randint(0, 605)
            self.rect.y = random.randint(0, 420)


    def main_menu_collision(self, collision):
        """Collision pour l'écran de début"""
        normal_vector = pg.math.Vector2()

        if self.rect.top < 3:  # haut
            normal_vector = pg.math.Vector2(0, -1)
            self.rect.top = 3
            collision = True

        if self.rect.left < 3:  # gauche
            normal_vector = pg.math.Vector2(1, 0)
            self.rect.left = 3
            collision = True

        if self.rect.right > self.display_width - 3:  # droite
            normal_vector = pg.math.Vector2(-1, 0)
            self.rect.right = self.display_width - 3
            collision = True

        if self.rect.top > DISPLAY_HEIGHT - 14:  
            normal_vector = pg.math.Vector2(0, 1)
            self.rect.top = DISPLAY_HEIGHT - 14
            collision = True

        if self.rect.x == 163 and 45 < self.rect.y < 59 + 294: 
            normal_vector = pg.math.Vector2(-1, 0)
            collision = True

        if self.rect.x == 177 + 251 and 45 < self.rect.y < 59 + 294: 
            normal_vector = pg.math.Vector2(1, 0)
            collision = True

        if self.rect.y == 45 and 162 < self.rect.x < 177 + 251: 
            normal_vector = pg.math.Vector2(0, -1)
            collision = True

        if self.rect.y == 59 + 294 and 162 < self.rect.x < 177 + 251: 
            normal_vector = pg.math.Vector2(0, 1)
            collision = True

        self.reflect(normal_vector) 

        return collision
        

    def main_menu(self):
        """Gère la balle dans l'écran de menu"""
        collision = False
        for _ in range(4):
            self.rect.x +=  self.orientation.x
            self.rect.y +=  self.orientation.y
            collision = self.main_menu_collision(collision)
        return collision


    def draw(self):
        
        self.screen.blit(self.img, pg.Rect(
            self.rect.x, self.rect.y, self.width, self.height))

        #pg.draw.rect(self.screen,(255,255,255),self.rect)
        if not(self.launched):
            pg.draw.line(self.screen, WHITE, (self.rect.x + self.width/2, self.rect.y + self.height/2),
                         (self.rect.x + self.width/2 + 50*self.orientation.x, self.rect.y + self.height/2 + 50*self.orientation.y))
