import neat
from constantes import *
from GameIA import GameIA
import pygame
import numpy as np
import os
import pickle
import random
import sys
import visualize



LEVEL_DE_TEST = "Levels/BigTest/BigTest_1.pickle"
NUMBER_OF_GENERATIONS = 80
MAXIMUM_TIME_AVAILABLE = 1000
LAST_TILE_HIT_TRESHOLD = 100

GENERATION = 0
NUMBER_OF_TRIES_PER_GENOME = 3
RENDERING = True
RANDOMIZE = True

DECR_STEP = 1 / (NUMBER_OF_TRIES_PER_GENOME + 0.1)


    

def eval_genome(genome):
    net = neat.nn.FeedForwardNetwork.create(genome[1], CONFIG)
    if not RANDOMIZE:
        game = GameIA.loadSceneFrom(net,pygame.display.set_mode((DISPLAY_WIDTH, DISPLAY_HEIGHT)),LEVEL_DE_TEST,RENDERING)
        return game.get_fitness()
    else:
        fitnesses = []
        for i in range(NUMBER_OF_TRIES_PER_GENOME):
            game = GameIA(net,rendering=RENDERING,decr=i*DECR_STEP)
            fitness = game.get_fitness()
            del game
            fitnesses.append(fitness)

        return np.mean(fitnesses)
    
def eval_genomes(genomes,config):
    global GENERATION
    for genome in genomes:
        
        genome[1].fitness = eval_genome(genome)
        #print(f'{GENERATION} {genome[0]} {genome[1].fitness}')

    GENERATION += 1
    

def run():
        


    # Create the population, which is the top-level object for a NEAT run.
    p = neat.Population(CONFIG)
    #p = neat.Checkpointer.restore_checkpoint("neat-checkpoint-99")

    # Add a stdout reporter to show progress in the terminal.
    p.add_reporter(neat.StdOutReporter(True))
    stats = neat.StatisticsReporter()
    p.add_reporter(stats)
    p.add_reporter(neat.Checkpointer(5))


    winner = p.run(eval_genomes, NUMBER_OF_GENERATIONS)
    # Display the winning genome.
    print('\nBest genome:\n{!s}'.format(winner))

    # Show output of the most fit genome against training data.
    print('\nOutput:')
    winner_net = neat.nn.FeedForwardNetwork.create(winner, CONFIG)

    visualize.draw_net(CONFIG, winner, True)
    visualize.plot_stats(stats, ylog=False, view=True)
    visualize.plot_species(stats, view=True)
    
    return winner




if __name__ == "__main__": 
    local_dir = os.path.dirname(__file__)

    RENDERING = len(sys.argv) > 1 and sys.argv[1] == '1'
    winner = run()
    print(winner.fitness)

    with open("AA","wb") as f:

        pickle.dump(winner,f)