import pygame as pg
import random
from constantes import *
import os
from AbstractBall import AbstractBall


class BalleMulti(AbstractBall):

    def __init__(self, x : float, y : float, barre1 , barre2, tiles, orientation=(0 , -1), launched : bool = False) -> None:
        super().__init__(x,y,tiles,orientation, launched)
        self._init_paddles([barre1,barre2])

    def _init_paddles(self, data):
        self.barre1 = data[0]
        self.barre2 = data[1]
        
        
    def collision_barre1(self):

        if not self.rect.colliderect(self.barre1.rect):
            return False,None

        else:
            self._reflect_on_paddle(self.barre1)
        
        return True,self.barre1
               
    def collision_barre2(self):

        if not self.rect.colliderect(self.barre2.rect):
            return False,None

        else:
            self._reflect_on_paddle(self.barre2)
        
        return True,self.barre2


    def collision_barre(self):
        data_coll_1 = self.collision_barre1()
        return data_coll_1 if data_coll_1[0] else self.collision_barre2()

    
   
    
    

    