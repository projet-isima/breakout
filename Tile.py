from typing import Optional
import pygame as pg
from constantes import *
import os


class Tile(pg.sprite.Sprite):

    RED = (255, 0, 0)
    GREEN = (0, 255, 0)
    BLUE = (0, 0, 255)
    YELLOW = (255, 255, 0)
    DARKISH_BLUE = (8, 30, 67)
    DARKISH_GREEN = (12, 51, 43)

    TILE_WIDTH = 50
    TILE_HEIGHT = 25

    LIFE = [1, 3, 4, 5]

    def __init__(self, screen: pg.surface.Surface, x: float, y: float, type: int, width: int, height: int) -> None:

        pg.sprite.Sprite.__init__(self)

        self.screen = screen  # l'écran sur lequel dessiné la tuile

        self.width = width  # largeur de la tuile
        self.height = height  # hauteur de la tuile

        self.type = type
        
        self._load_graphics(x, y)
        

        self.life = Tile.LIFE[self.type - 1]
        self.original_life = self.life

    def _load_graphics(self, x, y):
        tiles_directory = os.path.join("assets", "Tiles")

        self.image_name = os.path.join(
            tiles_directory, f"tuile{self.type}full.png")

        self.image_mid_name = os.path.join(
            tiles_directory, f"tuile{str(self.type)}mid.png"
        )

        self.image = pg.transform.scale(pg.image.load(self.image_name).convert(
        ).convert_alpha(), (self.TILE_WIDTH, self.TILE_HEIGHT))


        self.rect = self.image.get_rect() if self.image else None
        
        self.rect.x = x
        self.rect.y = y

    def getPosition(self) -> Optional[Position]:
        return (self.rect.x, self.rect.y) if self.rect else None

    def scaleY(self, factor: float) -> None:
        if self.image and self.rect:
            self.image = pg.transform.scale(
                self.image, (self.TILE_WIDTH, int(self.TILE_HEIGHT * factor)))
            self.rect.h = int(self.TILE_HEIGHT * factor)

    def draw(self) -> None:
        
        if self.life <= self.original_life // 2:
            self.image = pg.transform.scale(pg.image.load(self.image_mid_name).convert(
            ).convert_alpha(), (self.TILE_WIDTH, self.TILE_HEIGHT))

        if self.image and self.rect:
            self.screen.blit(self.image, self.rect)

    def getType(self) -> int:
        return self.type

    def getLife(self) -> int:
        return self.life
