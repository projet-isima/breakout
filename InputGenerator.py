from constantes import *

class InputGenerator:
    
    
    def __init__(self, balls, barres, items, tiles): 
        self._balls = balls 
        self._barres = barres 
        self._items = items
        self._tiles = tiles 
        
    def scale(self,input, maxValue):
        return input/maxValue

    def scale_X(self, abcis):
        return self.scale(abcis,DISPLAY_WIDTH)

    def scale_Y(self,ord):
        return self.scale(ord,DISPLAY_HEIGHT)

    def scale_ball_speed(self, ball_sp):
        return self.scale(ball_sp,self._balls[0].max_speed)
    
    def scale_player_speed(self, player_sp):
        return self.scale(player_sp,self._barre.vitesseMax)
    
    def scale_player_pos(self, X):
        return self.scale(X,DISPLAY_WIDTH)
    
    def get_number_of_tiles_alive(self): 
        numbers = [0 for _ in range(MAX_TILES_HOR)]
        
        for tile in self._tiles: 
            numbers[tile.getPosition()[0] // (TILE_WIDTH + GAP)] += 1
            
        return numbers
    
    
    def return_closest_item_info(self):
        """
            Retourne un coefficient pour l'item le plus proche
        """
        if len(self._items) == 0:
            return 0
        
        closest = self._items[0]
        min_distance = (closest.rect.x - self._barres[0].rect.x) ** 2 + (closest.rect.y - self._barres[0].rect.y) ** 2

        for item in self._items[1:]:
            distance = (item.rect.x - self._barres[0].rect.x) ** 2 + (item.rect.y - self._barres[0].rect.y) ** 2

            if distance < min_distance:
                min_distance = distance
                closest = item
        
        type_c = closest.getType()
        score = 1 / min_distance if min_distance > 0 else 0
        
        if closest.rect.x < self._barres[0].rect.x: 
            score *= -1
            
        if type_c == 1:
            score *= 3
        elif type_c == 2:
            score = 0
        elif type_c == 3:
            score *= 2
        elif type_c == 4:
            score *= 4.5

        return score

    def getClosestBall(self): 
        
        """Retourne la balle la plus prohce du joueur"""
        closest = self._balls[0]
        
        min_distance = (closest.rect.x - self._barres[0].rect.x) ** 2 + (closest.rect.y - self._barres[0].rect.y) ** 2

        for ball in self._balls[1:]:
            distance = (ball.rect.x - self._barres[0].rect.x) ** 2 + (ball.rect.y - self._barres[0].rect.y) ** 2
            
            if distance < min_distance: 
                closest = ball
                
        return closest

    def return_scaled_inputs(self):
        """ Retourne les inputs"""
        
        ball = self.getClosestBall()
        
        ball_position = ball.get_pos()
        
        ball_vel = ball.get_velocity()
        
        
        current_upgrade_score = 0
        active_upgrade = self._barres[0].get_active_upgrade()
        if active_upgrade == 1:
            current_upgrade_score = 50
        elif active_upgrade == 2: 
            current_upgrade_score = -30
        elif active_upgrade == 4:
            current_upgrade_score = 70

        return *ball_position,*ball_vel,self._barres[0].get_position()[0],self._barres[0].width,*self.get_number_of_tiles_alive(),self.return_closest_item_info(),active_upgrade