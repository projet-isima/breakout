import pygame
from State import State
import os
import platform
from constantes import *
from Button import Button
from LevelGestionState import LevelGestionState
from typing import Dict, Optional
import shutil
from LevelNamingState import LevelNamingState
from pygame import mixer


class LevelSelectionToEditState(State):

    def __init__(self, game) -> None:
        super().__init__(game)

        self.screen = pygame.display.get_surface()

        self._load_levels()

        self._state = 0

        self._return_button = Button((0, DISPLAY_HEIGHT-30), "retour.png", (60, 30))
        self._nouveau_button = Button(
            (DISPLAY_WIDTH-70, DISPLAY_HEIGHT-65), "nouveau.png", (70, 30))
        self._supprimer_button = Button(
            (DISPLAY_WIDTH-70, DISPLAY_HEIGHT-30), "supprimer.png", (70, 30))
        self._edit_button = Button(
            (DISPLAY_WIDTH-70, DISPLAY_HEIGHT-30), "reediter.png", (70, 30))

        self.bouton1 = Button(
            (50, 50), "Bouton_Selection.png", (180, 25))
        self.bouton2 = Button(
            (50, 85), "Bouton_Selection.png", (180, 25))
        self.bouton3 = Button(
            (50, 120), "Bouton_Selection.png", (180, 25))
        self.bouton4 = Button(
            (50, 155), "Bouton_Selection.png", (180, 25))
        self.bouton5 = Button(
            (50, 190), "Bouton_Selection.png", (180, 25))
        self.bouton6 = Button(
            (50, 225), "Bouton_Selection.png", (180, 25))
        self.bouton7 = Button(
            (50, 260), "Bouton_Selection.png", (180, 25))
        self.bouton8 = Button(
            (50, 295), "Bouton_Selection.png", (180, 25))
        self.bouton9 = Button(
            (50, 330), "Bouton_Selection.png", (180, 25))
            
        self._last_click = 0
        self._delay = 500

        self.image = pygame.image.load("assets/Ecran_edition.png")
        self.image = pygame.transform.scale(self.image, (250, 250))

    def _load_levels(self) -> None:

        level_filenames = [x[0].split() for x in os.walk("Levels")]

        self.levels: List[str] = []

        self._get_levels_from_level_path(level_filenames)

    def _get_levels_from_level_path(self, level_filenames):
        if platform.system() in ["Linux", "Darwin"]:
            self.levels = [x[0].split("/")[1] for x in level_filenames[1:]]
        elif platform.system() == "Windows":
            self.levels = [x[0].split("\\")[1] for x in level_filenames[1:]]

    def _introduce_level_naming(self) -> None:
        LevelNamingState(self.game).enter_stack()

    def getState(self) -> int:
        return self._state

    def setState(self, newState: int) -> None:
        self._state = newState = newState

    def _switch_state(self, click: Position) -> None:
        if self.getState() == 0 and self._supprimer_button.rect.collidepoint(*click):
            self.setState(1)

        elif self._state == 1 and self._edit_button.rect.collidepoint(*click):
            self.setState(0)

    def _can_click(self) -> bool:
        return pygame.time.get_ticks() - self._last_click > self._delay

    def _selections(self, pos: Position):
        
        """Gère les clics de la souris"""
        
        if self._return_button.rect.collidepoint(*pos):
            self.game.popStateStack()

        if self._nouveau_button.rect.collidepoint(*pos):
            SELECT_SOUND.play()
            if len(self.levels) < 9:
                self._introduce_level_naming()

        if (self._can_click()):
            self._last_click = pygame.time.get_ticks()
            SELECT_SOUND.play()
            self._switch_state(pos)

    def _removeLevel(self, selected: str) -> None:
        shutil.rmtree(os.path.join("Levels", selected))

    def _get_selected_scene(self, click: Position) -> Optional[str]:
        selected = None
        x, y = click
        for i, level in enumerate(self.levels):
            offset = len(level)*FONT_SIZE//2
            if (
                x >= DISPLAY_WIDTH // 2 - offset - 165
                and x <= DISPLAY_WIDTH // 2 + offset - 165
                and y >= i * TILE_HEIGHT + 50 + 10*i
                and y <= (i + 1) * TILE_HEIGHT + 50 + 10*i
            ):
                selected = level
        return selected

    def _introduce_level_gestion(self, selected: str) -> None:
        LevelGestionState(self.game, selected).enter_stack()
        
    def _process_left_click(self, click : Position) -> None:
        if selected := self._get_selected_scene(click):
            
            SELECT_SOUND.play()
            if self.getState() == 0:
                self._introduce_level_gestion(selected)

            else:
                self._removeLevel(selected)

        self._selections(click)
        

    def update(self, key_state: Dict[str, KeyData]) -> None:

        self._load_levels()

        if key_state["left_click"][0] == True:
            
            self._process_left_click(key_state["left_click"][1])
            

    def _render_selection_text(self) -> None:
        for i, level in enumerate(self.levels):
            color = (255, 0, 0)
            mouse_pos = pygame.mouse.get_pos()
            offset = len(level)*FONT_SIZE//2
            if (
                mouse_pos[0] >= DISPLAY_WIDTH // 2 - offset - 165
                and mouse_pos[0] <= DISPLAY_WIDTH // 2 + offset - 165
                and mouse_pos[1] >= i * TILE_HEIGHT + 50 + 10*i
                and mouse_pos[1] <= (i + 1) * TILE_HEIGHT + 50 + 10*i
            ):
                color = (0, 0, 0)
            self.game.draw_text(
                level, (DISPLAY_WIDTH//2-offset//2, i*TILE_HEIGHT), color, True)

    def render(self) -> None:

        pygame.draw.rect(self.screen, GREY, pygame.Rect(30, 30, 225, 350), 10)

        self.bouton1.render()
        self.bouton2.render()
        self.bouton3.render()
        self.bouton4.render()
        self.bouton5.render()
        self.bouton6.render()
        self.bouton7.render()
        self.bouton8.render()
        self.bouton9.render()

        for i in range(9):
            color = (0, 0, 0)
            mouse_pos = pygame.mouse.get_pos()

            if i < len(self.levels):
                offset = len(self.levels[i])*FONT_SIZE//2
                if (
                    mouse_pos[0] >= DISPLAY_WIDTH // 2 - offset - 165
                    and mouse_pos[0] <= DISPLAY_WIDTH // 2 + offset - 165
                    and mouse_pos[1] >= i * TILE_HEIGHT + 50 + 10*i
                    and mouse_pos[1] <= (i + 1) * TILE_HEIGHT + 50 + 10*i
                ):
                    color = (255, 255, 255)
                self.game.draw_text(self.levels[i], (DISPLAY_WIDTH//2-offset//2 - 165, i*TILE_HEIGHT + 50 + 10*i), color)
            else:
                offset = 3*FONT_SIZE//2
                self.game.draw_text("Vide", (DISPLAY_WIDTH//2-offset//2 - 165, i*TILE_HEIGHT + 50 + 10*i), color)

        self._return_button.render()

        pygame.draw.rect(self.screen, GREY, pygame.Rect(290, 65, 270, 270), 10)
        self.screen.blit(self.image, (300,75))

        self._nouveau_button.render()

        if self._state == 0:
            self._supprimer_button.render()
        else:
            self._edit_button.render()
