import itertools
import time
import math
import pygame
from Barre import Barre
from Balle import Balle
from Tile import Tile
from constantes import *
import pickle
from Particle import Particle
from Item import Item
import random
from AbstractGamePhase import AbstractGamePhase
from InputGenerator import InputGenerator


class GamePhaseIA(AbstractGamePhase):
    def __init__(self, game,tiles=None, lives = 3, allow_items=True,player_perk=None):
        super().__init__(game,tiles,lives,allow_items,player_perk)
        
        
    @classmethod
    def constructFromTiles(cls,game,tiles,player_perk=0):
        
        return GamePhaseIA(game,tiles,player_perk=player_perk)
    
    
    def _init_entities(self,player_perk=None):
        barre = Barre(self, self.display_width//2 -
                            60, self.display_height - 40)
        
        if player_perk != None:
            barre.receiveSignal(player_perk[0])
            
        ball = Balle(barre.rect.x + barre.width//2 - 7,
                           self.display_height - TILE_WIDTH - 4, barre, self._tiles)

        self._barres = [barre]
        self._balls = [ball]
        self.inputGen = InputGenerator(self._balls,self._barres,self._items,self._tiles)
    
    def _manage_ball_hit(self,hit):
        if hit and isinstance(hit, Tile):
            hit.life -= 1
        if hit and isinstance(hit, Tile) and hit.getLife() <= 0:
            self._tiles.remove(hit)
            self._score += hit.getType()

            self._process_spawn_items(hit)
            
    def _process_horizontal_movement(self, key_state):
        
        if key_state["right"][0] == True:
            self._barres[0].move("right")

        elif key_state["left"][0] == True:
            self._barres[0].move("left")
            
        else:
            self._barres[0].move("")

    def _process_pre_launch_ball_orientation(self, key_state):

        if key_state["up"][0] == True:
            self._balls[0].choose_ball_orientation("up")

        if key_state["down"][0] == True:
            self._balls[0].choose_ball_orientation("down")

        if key_state["space"][0] == True:
            self._prelaunch_on_space_pressed() 
        
        self._balls[0].rect.x = self._barres[0].rect.x + \
            self._barres[0].rect.w // 2 - self._balls[0].rect.w // 2

    def return_scaled_inputs(self):
        """Retourne les inputs du réseau de neuronnes"""
        return self.inputGen.return_scaled_inputs()