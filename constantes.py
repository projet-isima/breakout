#A remplir avec les constantes
import math
from typing import Tuple, List, Any
import neat
from pygame import mixer
import os

CONFIG = neat.Config(neat.DefaultGenome, neat.DefaultReproduction,
                        neat.DefaultSpeciesSet, neat.DefaultStagnation,
                        "configurationNeat.txt")

CONFIG_LAUCHER = neat.Config(neat.DefaultGenome, neat.DefaultReproduction,
                        neat.DefaultSpeciesSet, neat.DefaultStagnation,
                        "configurationNeatLauncher.txt")


CONFIG_ITEMS = neat.Config(neat.DefaultGenome, neat.DefaultReproduction,
                        neat.DefaultSpeciesSet, neat.DefaultStagnation,
                        "configurationNeatItems.txt")
CONE = math.pi / 6

mixer.init()


Position = Tuple[int,int]
Color = Tuple[int,int,int]
KeyData = Tuple[bool, Any]
Grid = List[List[int]]

RED = (255,0,0)
GREEN = (0,255,0)
BLUE = (0,0,255)
PURPLE = (128,0,128)

YELLOW = (255,255,0)
DARKISH_BLUE = (8,30,67)
DARKISH_GREEN = (12,51,43)
GREY = (124, 124, 124)
WHITE = (255,255,255)

NUMBER_OF_TYPES_OF_TILES = 4


FONT_SIZE = 20
TILE_WIDTH = 50
TILE_HEIGHT = 25
EXTRA_ROW = 7

GAP = 5
MAX_TILES_HOR = 11
MAX_TILES_VERT = 7
TOTAL_ROW = MAX_TILES_VERT + EXTRA_ROW

DISPLAY_WIDTH = (TILE_WIDTH  + GAP)* MAX_TILES_HOR 
DISPLAY_HEIGHT = (TILE_HEIGHT + GAP) * TOTAL_ROW 


MBUTTON_W = DISPLAY_WIDTH // 3
MBUTTON_H = math.floor(DISPLAY_HEIGHT // 12)

MBUTTON_X = (DISPLAY_WIDTH - MBUTTON_W) // 2
PLAY_Y = DISPLAY_HEIGHT // 10 * 2
PLAY_Y2 = DISPLAY_HEIGHT // 10 * 3
PLAY_Y3 = DISPLAY_HEIGHT // 10 * 4
PLAY_Y4 = DISPLAY_HEIGHT // 10 * 5
LEDI_Y = DISPLAY_HEIGHT // 10 * 6
QUIT_Y = DISPLAY_HEIGHT // 10 * 7

MIN_TEXT_RECT = 100
TEXT_HEIGHT = 40


FPS = 60

DARKISH_BLUE = (8,30,67)
DARKISH_GREEN = (12,51,43)

MIN_INPUT_WIDTH = 50
INPUT_RECT_PADDING_X = 5
INPUT_RECT_PADDING_Y = 5


SCENE_ONGOING = 3
SCENE_DEFEAT = 2
SCENE_VICTORY = 1
SCENE_PAUSE = 4



SELECT_SOUND = mixer.Sound(os.path.join("Music","Blip_Select18.wav"))
COIN_SOUND = mixer.Sound(os.path.join("Music","Pickup_Coin5.wav"))
LASER_SOUND = mixer.Sound(os.path.join("Music","Laser_Shoot.wav")) 
POWER_SOUND = mixer.Sound(os.path.join("Music","Powerup7.wav"))
HIT_SOUND = mixer.Sound(os.path.join("Music","Explosion6.wav"))

ITEM_SPRITES = {1:"expand.png",2:"shrink.png",3:"multipleBalls.png",4:"laser.png",5:"star.png"}