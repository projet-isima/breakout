import pygame 
from constantes import *
from State import State
from Item import Item
from Button import Button

PRICES = [3,1,10]

class ShopScene():
    
    
    def __init__(self,levelLoader):

        self.screen = pygame.display.get_surface()

        self.levelLoader = levelLoader
        self.choices = [Item(i,(i-1)*DISPLAY_WIDTH/3+40,DISPLAY_HEIGHT/3) for i in range(1,3)] + [Item(4,2*DISPLAY_WIDTH/3+40,DISPLAY_HEIGHT/3)]
        self.buttons = [Button(((i)*DISPLAY_WIDTH/3+40,DISPLAY_HEIGHT/1.85), "vide.png", (TILE_WIDTH*2,  MBUTTON_H)) for i in range(0,3)]
        self._status = SCENE_ONGOING
        self.star_expended = 0

        self.suivant = Button((DISPLAY_WIDTH/2-MBUTTON_W/2, DISPLAY_HEIGHT/1.25), "suivant.png", (MBUTTON_W, MBUTTON_H))

        self.star = pygame.image.load("assets/Items/star.png")
        self.star = pygame.transform.scale(self.star, (25, 25))
        self.last_purchase = 0
        self.delay_purchase = 500

    def getStatus(self):
        return self._status
    
    
    
    def get_number_of_stars(self): 
        return self.star_expended
    
    def update(self,key_state): 

        if (key_state["left_click"][0] == True): 
            for i,button in enumerate(self.buttons): 
                if button.rect.collidepoint(key_state["left_click"][1]) and self.levelLoader.actual_number_of_stars >= PRICES[i] and pygame.time.get_ticks() - self.last_purchase > self.delay_purchase:
                    self.star_expended += -PRICES[i]
                    self.levelLoader.perks[0] = self.choices[i].getType()
                    self.last_purchase = pygame.time.get_ticks()


            if self.suivant.rect.collidepoint(key_state["left_click"][1]):
                self._status = SCENE_VICTORY

        if (key_state["right_click"][0] == True):  #un peu pourri , c'est temporaire
            for i,button in enumerate(self.buttons): 
                if button.rect.collidepoint(key_state["right_click"][1]) and self.levelLoader.actual_number_of_stars >= PRICES[i] and len(self.levelLoader.perks) == 2:
                    if pygame.time.get_ticks() - self.last_purchase > self.delay_purchase:
                        self.star_expended += -PRICES[i]
                        self.levelLoader.perks[1] = self.choices[i].getType()
                        self.last_purchase = pygame.time.get_ticks()

            if self.suivant.rect.collidepoint(key_state["right_click"][1]):
                self._status = SCENE_VICTORY
            
        
    def render(self):

        self.suivant.render()

        for i,choice in enumerate(self.choices):
            choice.drawChoice()
            Button(((i)*DISPLAY_WIDTH/3+40,DISPLAY_HEIGHT/1.85), "vide.png", (TILE_WIDTH*2,  MBUTTON_H)).render()
            self.levelLoader.game.draw_text(f"{PRICES[i]}",(i * DISPLAY_WIDTH/3+50 + choice.rect.w // 2,DISPLAY_HEIGHT/1.80),(0,0,0))
            self.screen.blit(self.star, (i * DISPLAY_WIDTH/3+70 + choice.rect.w // 2,DISPLAY_HEIGHT/1.80-2))
            
        

