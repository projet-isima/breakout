from abc import ABC,abstractmethod

class State(ABC):
    """
    Classe abstraite

    Définit ce qu'est un état.
    Un état comprend les méthodes update, render et enterstate.
    Un état gère lui-même son affichage et sa mise à jour.

    Tous les états du jeu héritent de la classe State.
    """
    def __init__(self, game):
        self.game = game

    @abstractmethod
    def update(self,key_state) -> None:
        """Mise à jour de l'état interne"""
        ...
    
    @abstractmethod
    def render(self) -> None:
        """Affichage"""
        ...  

    def enter_stack(self) -> None:
        """Met l'état dans la pile du jeu"""
        self.game.stack(self)

