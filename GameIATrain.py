import itertools
import time
import math
import pygame
from Barre import Barre
from Balle import Balle
from Tile import Tile
from constantes import *
import pickle
from Particle import Particle
from Item import Item
import os
import random
import neat
from typing import Dict
import numpy as np
import warnings
from GameIA import GameIA
from InputGenerator import InputGenerator

class GameIATrain(GameIA):
    
    def __init__(self,net,screen = None ,tiles=None, lives = 3,rendering=False,decr=0,allow_items=True):
        super().__init__(net,screen = None ,tiles=None, lives = 3,rendering=rendering,decr=0,allow_items=allow_items)
        
        
        
    def _initialize(self, net, screen=None, tiles=None, lives=3, rendering=False, decr=0,allow_items=True):
        super()._initialize(net, screen, tiles, lives, rendering, decr,allow_items=allow_items)
        self.allow_items = allow_items
        self._items = [] 
        self.lasers = pygame.sprite.Group()
        
        self._barres[0].delay_shoot = 5
        self.number_of_stars_collected = 0
        self.inputGenerator = InputGenerator(self._balls,self._barres,self._items,self._tiles)

        
    @classmethod
    def loadSceneFrom(cls, net,screen, filename,rendering=False,allow_items=True):
        repr_tile = []
        with open(filename, "rb") as f:
            repr_tile = pickle.load(f)
        tiles = [
            Tile(
                screen,
                j * (TILE_WIDTH + GAP),
                i * (TILE_HEIGHT + GAP),
                repr_tile[i][j],
                TILE_WIDTH,
                TILE_HEIGHT,
            )
            for i, j in itertools.product(
                range(len(repr_tile)), range(len(repr_tile[0]))
            )
            if repr_tile[i][j] != 0
        ]

        return GameIATrain(net,tiles=tiles,rendering=rendering,allow_items=allow_items)


    def addABall(self):
        orientation = pygame.math.Vector2(random.random(), random.random())
        orientation = pygame.math.Vector2.normalize(orientation)

        if not self.hasLost():
            ball = Balle(self._balls[0].rect.x, self._balls[0].rect.y,
                        self._barres[0], self._tiles, orientation, True)
            self._balls.append(ball)

    def _process_in_game(self):
        if self._has_started:

            self._process_ball_logic_in_game()
            self._process_lasers_and_hits()
            self._process_items()

        self.updateEntities()
        
    def _process_items(self):
        for i in range(len(self._items)-1, -1, -1):
            if self._barres[0].isInCollisionWith(self._items[i]):
                self._barres[0].receiveSignal(self._items[i].getType())


                if (self._items[i].getType() ==5):
                    self.number_of_stars_collected += 1
                    self.fitness += 1
                elif (self._items[i].getType() == 3):

                    self.addABall()
                    self.addABall()

                    self.fitness += 5
                
                elif self._items[i].getType() == 2: 
                    self.fitness -= 3
                elif self._items[i].getType() == 1: 
                    self.fitness += 6
                elif self._items[i].getType() == 4:
                    self.fitness += 10
                    
                self._items.remove(self._items[i])
                
                

    def _process_lasers_and_hits(self):
        for laser in iter(self.lasers):
            hit = pygame.sprite.spritecollideany(laser, self._tiles)

            if hit is not None:
                self.lasers.remove(laser)
                del laser
                hit.life -= 1
                self.fitness += 0.5

                if hit.getLife() <= 0:
                    self.fitness += hit.getType()
                    self._tiles.remove(hit)
                    self._score += hit.getType()
                    self.fitness += hit.getType()
                    del hit
    
    def return_closest_item_info(self):
        """Retourne un coefficient selon les informations de l'item le plus proche"""
        if len(self._items) == 0:
            return 0
        
        closest = self._items[0]
        min_distance = (closest.rect.x - self._barre.rect.x) ** 2 + (closest.rect.y - self._barre.rect.y) ** 2

        for item in self._items[1:]:
            distance = (item.rect.x - self._barre.rect.x) ** 2 + (item.rect.y - self._barre.rect.y) ** 2

            if distance < min_distance:
                min_distance = distance
                closest = item
        
        type_c = closest.getType()
        score = 1 / min_distance if min_distance > 0 else 0
        
        if closest.rect.x < self._barre.rect.x: 
            score *= -1
            
        if type_c == 1:
            score *= 3
        elif type_c == 2:
            score = 0
        elif type_c == 3:
            score *= 2
        elif type_c == 4:
            score *= 4.5

        return score


    
  