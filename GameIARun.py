import itertools
import time
import math
import pygame
from Barre import Barre
from Balle import Balle
from Tile import Tile
from constantes import *
import pickle
from Particle import Particle
from Item import Item
import os
import random
import neat
from typing import Dict
import sys

LEVEL_DE_TEST = "Levels/BigTest/BigTest_1.pickle"

local_dir = os.path.dirname(__file__)


LIA_FILE = 'saveLauncher'

LIA = None
with open(LIA_FILE,"rb") as f:
    LIA = pickle.load(f)

LGAME_NET = neat.nn.FeedForwardNetwork.create(LIA, CONFIG_LAUCHER)

class GameIARun:

        
    def __init__(self,genome,screen = None ,tiles=None, lives = 3):

        pygame.init()
        self.screen = screen or pygame.display.set_mode((DISPLAY_WIDTH, DISPLAY_HEIGHT))

        self.display_width, self.display_height = self.screen.get_size()

        self._tiles = pygame.sprite.Group()
        self.genome = genome
        
        self.net = neat.nn.FeedForwardNetwork.create(genome, CONFIG)
        
        if tiles is None:
            for i in range(10):
                self._tiles.add(
                    Tile(self.screen, i*(TILE_WIDTH+GAP), 100, 1, TILE_WIDTH, TILE_HEIGHT))
        else:
            for i in range(len(tiles)):
                self._tiles.add(tiles[i])

        self._barre = Barre(self, self.display_width//2 -
                            60, self.display_height - 40)
       
        
        

        self._ball = Balle(self._barre.rect.x + self._barre.width//2 - 7,
                           self.display_height - TILE_WIDTH - 4, self._barre, self._tiles,launched=False)
        
        self._balls = [self._ball]

        self._has_started = False
        self._score = 0

        self._status = SCENE_ONGOING
        
        self.assets_dir = os.path.join("assets", "nk57_monospace")
        self.font = pygame.font.Font(os.path.join(
            self.assets_dir, "nk57-monospace-cd-sb.ttf"), FONT_SIZE)

       
        self.pause_delay = 500

        
        self.number_of_lives = 1
    
        self._clock = pygame.time.Clock()
        self.starting_time = pygame.time.get_ticks()

        self._key_states: Dict[str, KeyData] = {"left": (False, None), "right": (False, None), "up": (False, None), "down": (False, None), "space": (False, None), "escape": (
            False, None), "backspace": (False, None), "quit": (False, None), "left_click": (False, None), "right_click": (False, None), "characters": (False, None)}

        self.launcher = LGAME_NET
        self.game_loop(self.genome)

    def getStatus(self):
        return self._status
    
    def get_number_of_stars(self):
        return self.number_of_stars_collected
    
    def get_number_of_lives(self):
        return self.number_of_lives


    def get_tiles_barycenter(self):
        centerx = 0
        centery = 0
        
        if len(self._tiles ) > 0:
            for tile in self._tiles:
                centerx += tile.getPosition()[0]
                centery += tile.getPosition()[1]
            
            return centerx / len(self._tiles),centery/ len(self._tiles)
        return (-1,-1)
    
    
    def get_number_of_tiles_alive(self): 
        numbers = [0 for _ in range(MAX_TILES_HOR)]
        
        for tile in self._tiles: 
            numbers[tile.getPosition()[0] // (TILE_WIDTH + GAP)] += 1
            
        return numbers
    def get_quadrants_barycenter(self):
        center1_x = 0
        center1_y = 0
        nb1 = 0

        center2_x = 0
        center2_y = 0
        nb2 = 0

        center3_x = 0
        center3_y = 0
        nb3 = 0

        center4_x = 0
        center4_y = 0
        nb4 = 0

        for i,tile in enumerate(self._tiles):
            col = i % MAX_TILES_HOR
            row = i // MAX_TILES_HOR

            if (col < MAX_TILES_HOR // 2 and row < MAX_TILES_HOR //2):
                nb1 += 1 
                center1_x += tile.getPosition()[0]
                center1_y += tile.getPosition()[1]
            elif (col >= MAX_TILES_HOR // 2 and row < MAX_TILES_HOR //2) :
                nb2 += 1 
                center2_x += tile.getPosition()[0]
                center2_y += tile.getPosition()[1]
            elif col < MAX_TILES_HOR // 2:
                nb3 += 1 
                center3_x += tile.getPosition()[0]
                center3_y += tile.getPosition()[1]
            else:
                nb4 += 1
                center4_x += tile.getPosition()[0]
                center4_y += tile.getPosition()[1]

        if nb1 != 0:
            center1_x = center1_x / nb1
            center1_y = center1_y / nb1
        
        if nb2 != 0:
            center2_x = center2_x / nb2
            center2_y = center2_y / nb2
        if nb3 != 0:
            center3_x = center3_x / nb3
            center3_y = center3_y / nb3
        if nb4 != 0:
            center4_x = center4_x / nb4
            center4_y = center1_y / nb4
            
        return center1_x, center1_y, center2_x, center2_y, center3_x, center3_y,center4_x, center4_y
        #return self.scale_X(center1_x), self.scale_X(center2_x), self.scale_X(center3_x),self.scale_X(center4_x)
            
    @classmethod
    def loadSceneFrom(cls, genome,screen, filename):
        repr_tile = []
        with open(filename, "rb") as f:
            repr_tile = pickle.load(f)
        tiles = [
            Tile(
                screen,
                j * (TILE_WIDTH + GAP),
                i * (TILE_HEIGHT + GAP),
                repr_tile[i][j],
                TILE_WIDTH,
                TILE_HEIGHT,
            )
            for i, j in itertools.product(
                range(len(repr_tile)), range(len(repr_tile[0]))
            )
            if repr_tile[i][j] != 0
        ]

        return GameIARun(genome,tiles=tiles)


    def scale(self,input, maxValue):
        return input/maxValue

    def scale_X(self, abcis):
        return self.scale(abcis,DISPLAY_WIDTH)

    def scale_Y(self,ord):
        return self.scale(ord,DISPLAY_HEIGHT)

    def scale_ball_speed(self, ball_sp):
        return self.scale(ball_sp,self._ball.max_speed)
    
    def scale_player_speed(self, player_sp):
        return self.scale(player_sp,self._barre.vitesseMax)
    
    def scale_player_pos(self, X):
        return self.scale(X,DISPLAY_WIDTH)
        
    def return_scaled_inputs(self):
        ball_position = self._ball.get_pos()
        ball_position_sc = (self.scale_X(ball_position[0]),self.scale_Y(ball_position[1]))
        
        ball_vel = self._ball.get_velocity()
        ball_vel_sc = (self.scale_ball_speed(ball_vel[0]),self.scale_ball_speed(ball_vel[1]))
                            
        player_pos_sc = self.scale_player_pos(self._barre.get_position()[0])
        
                   
        bars = self.get_quadrants_barycenter()
        bars_sc = []                  
        for i,coord in enumerate(bars):
            if i % 2 == 0:
                bars_sc.append(self.scale_X(coord))
            else:
                bars_sc.append(self.scale_Y(coord))
                
        return *ball_position,*ball_vel,self._barre.get_position()[0],*self.get_number_of_tiles_alive()
        
    def _get_vision(self):   
        dir = pygame.math.Vector2(*self._balls[0].get_orientation())
        number_seen = 0 
        for tile in self._tiles:
            ball_to_tile = -pygame.math.Vector2(*self._balls[0].rect.center) + pygame.math.Vector2(*tile.rect.center)
            acosine = dir.dot(ball_to_tile) / (ball_to_tile.magnitude())
            angle = math.acos(acosine)
            if angle <= CONE/2: 
                number_seen += 1
            
        return number_seen
       
       
    def game_loop(self,genome):
        self.genome = genome
        
        while self._status == SCENE_ONGOING:
            if self._has_started:
                output = self.net.activate(self.return_scaled_inputs())
                if (output[0] >= output[1] and output[0] >= output[2]):
                    self._key_states["right"] = True,None
                elif (output[1] >= output[2] and output[1] >= output[0]):
                    self._key_states["left"] = True,None
            else:
                self._key_states["space"] = True,None
                
                """if (len(self._tiles) > 0):
                    percent_seen = self._get_vision()/len(self._tiles)
                else: 
                    percent_seen = 1
                output = self.launcher.activate((self._barre.rect.x,*self.get_number_of_tiles_alive(),percent_seen))
                if (output[0] >= output[1] and output[0] >= output[2]):
                    self._key_states["up"] = True,None
                elif (output[1] >= output[2] and output[1] >= output[0]):
                    self._key_states["down"] = True,None
                else:
                    self._key_states["space"] = True,None    
                    
                if (output[3] >= output[4] and output[3] >= output[5]):
                    self._key_states["right"] = True,None
                elif (output[4] >= output[5] and output[4] >= output[3]):
                    self._key_states["left"] = True,None"""
               
            self._process_in_game()
            self.update()
            self.render()
            self.reset_keys()
            
    def _process_horizontal_movement(self, key_state):
        
        if key_state["right"][0] == True:
            self._barre.move("right")
            

        elif key_state["left"][0] == True:
            self._barre.move("left")
            
        else:
            self._barre.move("")
            
    def _process_pre_launch_ball_orientation(self, key_state):

        if key_state["up"][0] == True:
            self._ball.choose_ball_orientation("up")

        if key_state["down"][0] == True:
            self._ball.choose_ball_orientation("down")

        if key_state["space"][0] == True:
            self._has_started = True
            self._ball.launch()
            self._barre.inplay = True
            
        
        self._balls[0].rect.x = self._barre.rect.x + \
            self._barre.rect.w // 2 - self._balls[0].rect.w // 2

    def _remove_offscreen_balls(self):
        for ball in self._balls[::-1]:
            if ball.isOffScreen():
                self._balls.remove(ball)


    def _process_ball_logic_in_game(self):
        for ball in self._balls:
            collision_happened, hit = ball.move()



            if hit and isinstance(hit, Tile):
                hit.life -= 1
                
            if hit and isinstance(hit, Tile) and hit.getLife() <= 0:
                self._tiles.remove(hit)


                self._score += hit.getType()

                del hit

            self._remove_offscreen_balls()

   
    def _process_in_game(self):
        if self._has_started:

            self._process_ball_logic_in_game()
           

        self.updateEntities()

    def _pause(self): 
        self.last_pause = pygame.time.get_ticks()
        self._status = SCENE_PAUSE if self._status != SCENE_PAUSE else SCENE_ONGOING
        
        
        

    def update(self):
        pygame.key.set_repeat(10,10)
        if (
            self._key_states["escape"][0] == True
            and pygame.time.get_ticks() - self.last_pause >= self.pause_delay
        ):
            self._pause()

        if self._status != SCENE_PAUSE:
            self._process_horizontal_movement(self._key_states)

            if not(self._has_started):
                self._process_pre_launch_ball_orientation(self._key_states)

            self._process_in_game()
        
            if self.hasLost():
                self.number_of_lives -= 1
                if self.number_of_lives == 0:
                    self._status = SCENE_DEFEAT
                    
                else:
                    self._barre = Barre(self, self.display_width//2 -
                                60, self.display_height - 40)
                    self._ball = Balle(self._barre.rect.x + self._barre.width//2 - 7,
                                    self.display_height - TILE_WIDTH - 4, self._barre, self._tiles)

                    self._balls = [self._ball]
                

            elif self.hasWon():
                self._status = SCENE_VICTORY
                self.loadWinScreenAndExit()

   
    def spawnParticlesAt(self, pos: Position):
        for _ in range(math.floor(self._ball.getSpeed() * 10)):
            self._particle_array.append(Particle(2, *pos))

    def hasLost(self):
        return len(self._balls) == 0 
    def loadLoseScreenAndExit(self):
        defImg = pygame.image.load("./assets/defaite.png")
        defImg = pygame.transform.scale(
            defImg, (DISPLAY_WIDTH, DISPLAY_HEIGHT))

        self.screen.blit(defImg, (0, 0))
        pygame.display.flip()

    def loadWinScreenAndExit(self):
        vicImg = pygame.image.load("./assets/victoire.png")
        vicImg = pygame.transform.scale(
            vicImg, (DISPLAY_WIDTH, DISPLAY_HEIGHT))
        self.screen.blit(vicImg, (0, 0))

        pygame.display.flip()

    def getScore(self):
        return self._score

    def updateEntities(self):

        self._ball.update()

        self._barre.update()

        
        
    def drawEntities(self):

        for tile in self._tiles:
            tile.draw()

        for ball in self._balls:
            ball.draw()
        
        self._barre.draw()

    
            
        
    def hasWon(self):
        return len(self._tiles) == 0
    
    def draw_text(
        self, text: str, position: Position, color: Color,
        show_rect: bool = False, rot: bool = False
    ) -> None:

        rect = pygame.Rect(position[0], position[1],
                           MIN_TEXT_RECT, TEXT_HEIGHT)
        text_surface = self.font.render(text, True, color)

        if rot:
            text_surface = pygame.transform.rotate(text_surface, rot)

        rect.w = min(MIN_TEXT_RECT, text_surface.get_width()+5)
        if show_rect:
            pygame.draw.rect(self.screen, (255, 255, 255), rect)
        self.screen.blit(text_surface, rect)

    def render(self):
        self.screen.fill((0,0,0))
        
        if self._status == SCENE_DEFEAT:
            self.loadLoseScreenAndExit()
        elif self._status == SCENE_VICTORY:
            self.loadWinScreenAndExit()
        else:
            
            self.drawEntities()
            
        self._clock.tick(FPS)
        pygame.display.flip()
    
    def reset_keys(self) -> None:
        """Remettre l'état des touches à leur valeur de base"""
        for key in self._key_states.keys():
            self._key_states[key] = (False, None)


if __name__ == "__main__": 
    
    winner = None
    
    
    
    if len(sys.argv) > 1:
    
        with open(sys.argv[1],"rb") as f:
            winner = pickle.load(f)
        
        
        GameIARun.loadSceneFrom(winner,pygame.display.set_mode((DISPLAY_WIDTH, DISPLAY_HEIGHT)),LEVEL_DE_TEST)
    

    else:
        print("Give the file name,please")