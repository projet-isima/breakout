import os
from typing import Dict, Union

import pygame

from constantes import *
from GamePhaseMulti import GamePhaseMulti
from AbstractLevelLoader import AbstractLevelLoader
from State import State
from Item import Item
import pickle
from GamePhaseMultiIA import GamePhaseMultiIA

IA_FILE = 'IAModels/Items/ItemLord/ItemLord'


class LevelLoaderMultiIA(AbstractLevelLoader):
    def __init__(self, game, levelname: str,shopAvailable=True) -> None:
        super().__init__(game,levelname, shopAvailable)
        self.levelname = levelname
        self.screen = pygame.display.get_surface()

        with open(IA_FILE,"rb") as f:
            IA = pickle.load(f)
            
        self.net = neat.nn.FeedForwardNetwork.create(IA, CONFIG_ITEMS)

    def _init_perks(self):
        return [0,0]
    
    def _load_next_game_scene(self):
        self.actualScene = GamePhaseMultiIA.loadSceneFrom(self.game,self.screen, os.path.join(
            "Levels", self.levelname, self.scenes[self.selected_index]),self.perks)
        self.selected_index += 1
        self.perks = self._init_perks()
        
        return self.actualScene
    
    
    def _update_keystate(self,keystate):
        for key in keystate.keys():
            if key not in ["escape", "up", "down", "left", "right", "space","left_click","right_click","s","z"]:
                keystate[key] = False,None

        if self._isGamePhase(self.actualScene) and self.actualScene._has_started:
            output = self.net.activate(self.actualScene.return_scaled_inputs())

            if (output[0] >= output[1] and output[0] >= output[2]):
                keystate["d"] = True,None
            elif (output[1] >= output[2] and output[1] >= output[0]):
                keystate["q"] = True,None

       
    def _isGamePhase(self, scene):
        return isinstance(scene,GamePhaseMultiIA)