import itertools
import time
import math
import pygame
from Barre import Barre
from Balle import Balle
from Tile import Tile
from constantes import *
import pickle
from Particle import Particle
from Item import Item
import os
import random
import neat
from typing import Dict
import numpy as np
import warnings
from GameIA import GameIA

IA_FILE = 'saveIA2'
IA = None
with open(IA_FILE,"rb") as f:
    IA = pickle.load(f)

GAME_NET = neat.nn.FeedForwardNetwork.create(IA, CONFIG)

class GameLauncherTrain(GameIA):
    
    def __init__(self,net,screen = None ,tiles=None, lives = 3,rendering=False,decr=0):
        pygame.init()
        super().__init__(net,screen,tiles,lives,rendering,decr,False)
        
        
    def _initialize(self, net, screen=None, tiles=None, lives=3, rendering=False, decr=0,allow_items=False):
        
        super()._initialize(net, screen, tiles, lives, rendering, decr)
        self._balls[0].launched = False
        self.gaming_net = GAME_NET
        self._has_started = False
        
        
    def initializeTilesFromRepr(self,decr) -> None:
        def generate_random_repr():  
            representation = []

            for _ in range(MAX_TILES_VERT):
                line = []
                for _ in range(MAX_TILES_HOR):
                    if random.random() < 1 - decr:
                        line.append(random.randint(0,4))
                    else:
                        line.append(0)
                representation.append(line)
            
            return representation
        
        
        tiles = []
        representativeArray = generate_random_repr()
        for i, j in itertools.product(range(len(representativeArray)), range(len(representativeArray[0]))):
            type_of_tile = representativeArray[i][j]
            if type_of_tile != 0:
                tile = self.createTileAtCoords(j,i , type_of_tile)
                tiles.append(tile)

        return tiles
    
    
    
    def hasLost(self):
        return (len(self._balls) == 0  or pygame.time.get_ticks() - self.starting_time > 500) 
               
    def _get_vision(self):   
        dir = pygame.math.Vector2(*self._balls[0].get_orientation())
        number_seen = 0 
        for tile in self._tiles:
            ball_to_tile = -pygame.math.Vector2(*self._balls[0].rect.center) + pygame.math.Vector2(*tile.rect.center)
            acosine = dir.dot(ball_to_tile) / (ball_to_tile.magnitude())
            angle = math.acos(acosine)
            if angle <= CONE/2: 
                number_seen += 1
            
        return number_seen
        

    def game_loop(self):
        
        while self._status == SCENE_ONGOING:
            
            if not self._has_started:
                if (len(self._tiles) > 0):
                    percent_seen = self._get_vision()/len(self._tiles)
                else: 
                    percent_seen = 1
                output = self.net.activate((self._barre.rect.x,*self.get_number_of_tiles_alive(),percent_seen))
                if (output[0] >= output[1] and output[0] >= output[2]):
                    self._key_states["up"] = True,None
                elif (output[1] >= output[2] and output[1] >= output[0]):
                    self._key_states["down"] = True,None
                else:
                    if pygame.time.get_ticks() - self.starting_time > 30:
                        self.fitness -= 100
                    
                    self.fitness += percent_seen * 1000
                    
                    self._key_states["space"] = True,None    
                    
                if (output[3] >= output[4] and output[3] >= output[5]):
                    self._key_states["right"] = True,None
                elif (output[4] >= output[5] and output[4] >= output[3]):
                    self._key_states["left"] = True,None
               
            else: 
                output = self.gaming_net.activate(self.return_scaled_inputs())
            
            
                if (output[0] >= output[1] and output[0] >= output[2]):
                    self._key_states["right"] = True,None
                elif (output[1] >= output[2] and output[1] >= output[0]):
                    self._key_states["left"] = True,None
               
                    
                    
                            
            self._process_in_game()
            self.update()
            if self.rendering:
                self.render()
            
            self.reset_keys()