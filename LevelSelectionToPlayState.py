import pygame
from State import State
import os
import platform
from constantes import *
from LevelLoader import LevelLoader
from Button import Button
from typing import Dict


class LevelSelectionToPlayState(State):

    def __init__(self, game) -> None:
        super().__init__(game)

        self.screen = pygame.display.get_surface()

        self.bouton1 = Button(
            (50, 50), "Bouton_Selection.png", (180, 25))
        self.bouton2 = Button(
            (50, 85), "Bouton_Selection.png", (180, 25))
        self.bouton3 = Button(
            (50, 120), "Bouton_Selection.png", (180, 25))
        self.bouton4 = Button(
            (50, 155), "Bouton_Selection.png", (180, 25))
        self.bouton5 = Button(
            (50, 190), "Bouton_Selection.png", (180, 25))
        self.bouton6 = Button(
            (50, 225), "Bouton_Selection.png", (180, 25))
        self.bouton7 = Button(
            (50, 260), "Bouton_Selection.png", (180, 25))
        self.bouton8 = Button(
            (50, 295), "Bouton_Selection.png", (180, 25))
        self.bouton9 = Button(
            (50, 330), "Bouton_Selection.png", (180, 25))

        level_filenames = [x[0].split() for x in os.walk("Levels")]

        self.levels: List[str] = []

        if platform.system() in ["Linux", "Darwin"]:
            self.levels = [x[0].split("/")[1] for x in level_filenames[1:]]
        elif platform.system() == "Windows":
            self.levels = [x[0].split("\\")[1] for x in level_filenames[1:]]

        self.selected = ""
        self.scene_index = 0

        self.return_button = Button((0, DISPLAY_HEIGHT-30), "retour.png", (60, 30))

        self.image = pygame.image.load("assets/Ecran_selection.png")
        self.image = pygame.transform.scale(self.image, (250, 250))
        
        
    def _get_selected(self,click: Position):
        x, y = click

        selected = None
        for i, level in enumerate(self.levels):
            offset = len(level) * FONT_SIZE//2
            if (
                x >= DISPLAY_WIDTH // 2 - offset - 165
                and x <= DISPLAY_WIDTH // 2 + offset - 165
                and y >= i * TILE_HEIGHT + 50 + 10*i
                and y <= (i + 1) * TILE_HEIGHT + 50 + 10*i
            ):
                selected = level
        
        return selected
        
        
    def _introduce_levelLoader(self,selected):
        levelLoader = LevelLoader(self.game, selected)

        levelLoader.enter_stack()
        
    def _process_left_click(self, click : Position) -> None:
        
        if selected := self._get_selected(click):
            SELECT_SOUND.play()
            self._introduce_levelLoader(selected)
            

        if self.return_button.rect.collidepoint(click):
            SELECT_SOUND.play()
            self.game.popStateStack()



    def update(self, key_state: Dict[str, KeyData]) -> None:
        if len(self.levels) == 0:
            self.game.popStateStack()

        if key_state["left_click"][0] == True:
            self._process_left_click(key_state["left_click"][1])
            
    def render(self) -> None:

        pygame.draw.rect(self.screen, GREY, pygame.Rect(30, 30, 225, 350), 10)

        self.bouton1.render()
        self.bouton2.render()
        self.bouton3.render()
        self.bouton4.render()
        self.bouton5.render()
        self.bouton6.render()
        self.bouton7.render()
        self.bouton8.render()
        self.bouton9.render()

        for i in range(9):
            color = (0, 0, 0)
            mouse_pos = pygame.mouse.get_pos()

            if i < len(self.levels):
                offset = len(self.levels[i])*FONT_SIZE//2
                if (
                    mouse_pos[0] >= DISPLAY_WIDTH // 2 - offset - 165
                    and mouse_pos[0] <= DISPLAY_WIDTH // 2 + offset - 165
                    and mouse_pos[1] >= i * TILE_HEIGHT + 50 + 10*i
                    and mouse_pos[1] <= (i + 1) * TILE_HEIGHT + 50 + 10*i
                ):
                    color = (255, 255, 255)
                self.game.draw_text(self.levels[i], (DISPLAY_WIDTH//2-offset//2 - 165, i*TILE_HEIGHT + 50 + 10*i), color)
            else:
                offset = 3*FONT_SIZE//2
                self.game.draw_text("Vide", (DISPLAY_WIDTH//2-offset//2 - 165, i*TILE_HEIGHT + 50 + 10*i), color)

        self.return_button.render()

        pygame.draw.rect(self.screen, GREY, pygame.Rect(290, 65, 270, 270), 10)
        self.screen.blit(self.image, (300,75))
