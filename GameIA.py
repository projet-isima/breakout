import itertools
import time
import math
import pygame
from Barre import Barre
from Balle import Balle
from Tile import Tile
from constantes import *
import pickle
from Particle import Particle
from Item import Item
import os
import random
import neat
from typing import Dict
import numpy as np
import warnings

from InputGenerator import InputGenerator

from GamePhaseIA import GamePhaseIA
from AbstractGamePhase import AbstractGamePhase
warnings.filterwarnings('ignore')


LEVEL_DE_TEST = "Levels/TESTIA/TESTIA_1.pickle"
NUMBER_OF_GENERATIONS = 20
MAXIMUM_TIME_AVAILABLE = FPS * 60 * 3
LAST_TILE_HIT_TRESHOLD = 100

GENERATION = 0


class GameIA:
        
    def __init__(self,net,screen = None ,tiles=None, lives = 3,rendering=False,decr=0,allow_items=False):
        
        self._initialize(net,screen,tiles,lives,rendering,decr,allow_items)
        
        self.game_loop()
        
        
    
    def _initialize(self,net,screen = None ,tiles=None, lives = 3,rendering=False,decr=0,allow_items=False):
        pygame.init()
        self.screen = screen or pygame.display.set_mode((DISPLAY_WIDTH, DISPLAY_HEIGHT))

        self.display_width, self.display_height = DISPLAY_WIDTH,DISPLAY_HEIGHT

        self.fitness = 0.0
        self.net = net
        
        self.rendering = rendering
    
        self._tiles = pygame.sprite.Group()
        if tiles:
            for i in range(len(tiles)):
                self._tiles.add(tiles[i])
        else:
            self._tiles = self.initializeTilesFromRepr(decr)


        self._has_started = True
        self._score = 0

        self._status = SCENE_ONGOING
        
        self.assets_dir = os.path.join("assets", "nk57_monospace")
        self.font = pygame.font.Font(os.path.join(
            self.assets_dir, "nk57-monospace-cd-sb.ttf"), FONT_SIZE)
        
        self.score = 0
        self.pause_delay = 500

        self.hits = 0
        self.multiplier = 3
        self.number_of_lives = 1
    
        self._clock = pygame.time.Clock()
        self.starting_time = pygame.time.get_ticks()
        
        self.starRate = 0.5
        self.itemSpawnRate = 0.6

        self._key_states: Dict[str, KeyData] = {"left": (False, None), "right": (False, None), "up": (False, None), "down": (False, None), "space": (False, None), "escape": (
            False, None), "backspace": (False, None), "quit": (False, None), "left_click": (False, None), "right_click": (False, None), "characters": (False, None)}

        self.last_tile_hit = 0
        
        self._init_entities()
        
        
    def _init_entities(self):
        
        barre = Barre(self, self.display_width//2 -
                            60, self.display_height - 40)

       
        self._barres = [barre]
        
        angle =  -math.pi / 16 - random.random() *(math.pi - math.pi/8)
        orientation = (math.cos(angle),math.sin(angle))

        self._ball = Balle(self._barres[0].rect.x + self._barres[0].width//2 - 7,
                           self.display_height - TILE_WIDTH - 4, self._barres[0], self._tiles,orientation, launched=True)
        
        self._balls = [self._ball]
        self._items = []
        
        self.inputGenerator = InputGenerator(self._balls,self._barres,self._items,self._tiles)


    
    def initializeTilesFromRepr(self,decr) -> None:
        def generate_random_repr():  
            representation = []

            for _ in range(MAX_TILES_VERT-1):
                line = []
                for _ in range(MAX_TILES_HOR):
                    if random.random() < 1 - decr:
                        line.append(random.randint(0,4))
                    else:
                        line.append(0)
                representation.append(line)
            line = [random.randint(1,4) for _ in range(MAX_TILES_HOR)]
            representation.append(line)
            return representation
        
        
        tiles = []
        representativeArray = generate_random_repr()
        for i, j in itertools.product(range(len(representativeArray)), range(len(representativeArray[0]))):
            type_of_tile = representativeArray[i][j]
            if type_of_tile != 0:
                tile = self.createTileAtCoords(j,i , type_of_tile)
                tiles.append(tile)

        return tiles
    
    def createTileAtCoords(self, column: int, row: int, type: int) -> Tile:

        return Tile(self.screen, column * (TILE_WIDTH + GAP), row * (TILE_HEIGHT + GAP), type, TILE_WIDTH, TILE_HEIGHT)


    def get_fitness(self):
        return self.fitness
    
    def getStatus(self):
        return self._status
    
    def get_number_of_stars(self):
        return self.number_of_stars_collected
    
    def get_number_of_lives(self):
        return self.number_of_lives


    @classmethod
    def loadSceneFrom(cls, net,screen, filename,rendering=False):
        repr_tile = []
        with open(filename, "rb") as f:
            repr_tile = pickle.load(f)
        tiles = [
            Tile(
                screen,
                j * (TILE_WIDTH + GAP),
                i * (TILE_HEIGHT + GAP),
                repr_tile[i][j],
                TILE_WIDTH,
                TILE_HEIGHT,
            )
            for i, j in itertools.product(
                range(len(repr_tile)), range(len(repr_tile[0]))
            )
            if repr_tile[i][j] != 0
        ]

        return GameIA(net,tiles=tiles,rendering=rendering)

    def return_scaled_inputs(self):
        return self.inputGenerator.return_scaled_inputs()
        
       
    def game_loop(self):
        
        while self._status == SCENE_ONGOING:
            output = self.net.activate(self.return_scaled_inputs())
            
            
            if (output[0] >= output[1] and output[0] >= output[2]):
                self._key_states["right"] = True,None
            elif (output[1] >= output[2] and output[1] >= output[0]):
                self._key_states["left"] = True,None
            
            
            
            self._process_in_game()
            self.update()
            if self.rendering:
                self.render()
            
            self.reset_keys()
            
    def _process_horizontal_movement(self, key_state):
        
        if key_state["right"][0] == True:
            self._barres[0].move("right")
            

        elif key_state["left"][0] == True:
            self._barres[0].move("left")
            
        else:
            self._barres[0].move("")
            
        

    def _process_pre_launch_ball_orientation(self, key_state):

        if key_state["up"][0] == True:
            self._ball.choose_ball_orientation("up")

        if key_state["down"][0] == True:
            self._ball.choose_ball_orientation("down")

        if key_state["space"][0] == True:
            self._has_started = True
            self._barres[0].inplay = True
            self._ball.launch()
            
        
        self._balls[0].rect.x = self._barres[0].rect.x + \
            self._barres[0].rect.w // 2 - self._balls[0].rect.w // 2

    def _remove_offscreen_balls(self):
        for ball in self._balls[::-1]:
            if ball.isOffScreen():
                self._balls.remove(ball)

    def _process_spawn_items(self,hit):
        if self.allow_items and (random.random() < self.itemSpawnRate):
            self.spawnItemAtLocation(*hit.getPosition())

    
    def spawnItemAtLocation(self, x, y):
        if random.random() < self.starRate: 
            self._items.append(Item(5,x,y))
        else:
            type_of_item = random.randint(1, 4)
            self._items.append(Item(type_of_item, x, y))

    def _process_ball_logic_in_game(self):
        for ball in self._balls:
            collision_happened, hit = ball.move()

            if hit and isinstance(hit, Tile):
                hit.life -= 1
            if hit:
                if isinstance(hit,Tile):
                    self.last_tile_hit = pygame.time.get_ticks()
                    self.fitness += 3
                    self.hits += 1
                    
                    
                    if  hit.getLife() <= 0:
                        self._tiles.remove(hit)
                        self.fitness += hit.getType()
                        self._score += hit.getType() * 2

                    self._process_spawn_items(hit)
                    
                    
                elif isinstance(hit,Barre):
                    self.hits = 0
                    
                

            self._remove_offscreen_balls()

   
    def _process_in_game(self):
        if self._has_started:

            self._process_ball_logic_in_game()
            #self._process_lasers_and_hits()
            #self._process_items()

        self.updateEntities()

    def _pause(self): 
        self.last_pause = pygame.time.get_ticks()
        self._status = SCENE_PAUSE if self._status != SCENE_PAUSE else SCENE_ONGOING
        
    
        
        

    def update(self):
        pygame.key.set_repeat(10,10)
        if (
            self._key_states["escape"][0] == True
            and pygame.time.get_ticks() - self.last_pause >= self.pause_delay
        ):
            self._pause()

        if self._status != SCENE_PAUSE:
            self._process_horizontal_movement(self._key_states)

            if not(self._has_started):
                self._process_pre_launch_ball_orientation(self._key_states)

            self._process_in_game()
        
            if self.hasLost():
                self.number_of_lives -= 1
                if self.number_of_lives == 0:
                    
                    self._status = SCENE_DEFEAT
                    
                self._init_entities()
                
                if pygame.time.get_ticks() - self.starting_time > MAXIMUM_TIME_AVAILABLE:
                    self.fitness -= 5000
                    
                    
            elif self.hasWon():
                self._status = SCENE_VICTORY
                self.fitness += 20000
            elif self._has_started: 
                self.fitness += 0.1
            
   
    def spawnParticlesAt(self, pos: Position):
        for _ in range(math.floor(self._ball.getSpeed() * 10)):
            self._particle_array.append(Particle(2, *pos))

    def hasLost(self):
        maxTime = False
        if pygame.time.get_ticks() - self.starting_time > MAXIMUM_TIME_AVAILABLE:
            maxTime = True
            self.starting_time = pygame.time.get_ticks()
        return len(self._balls) == 0  or maxTime
    
    def loadLoseScreenAndExit(self):
        defImg = pygame.image.load("./assets/defaite.png")
        defImg = pygame.transform.scale(
            defImg, (DISPLAY_WIDTH, DISPLAY_HEIGHT))

        self.screen.blit(defImg, (0, 0))
        pygame.display.flip()

    def loadWinScreenAndExit(self):
        vicImg = pygame.image.load("./assets/victoire.png")
        vicImg = pygame.transform.scale(
            vicImg, (DISPLAY_WIDTH, DISPLAY_HEIGHT))
        self.screen.blit(vicImg, (0, 0))

        pygame.display.flip()

    def getScore(self):
        return self._score

    def updateEntities(self):

        for ball in self._balls:
            ball.update()

        self._update_players()
                

        if self.allow_items:
            for i in range(len(self._items)-1, -1, -1):
                if (self._items[i].offScreen()):
                    p = self._items[i]
                    self._items.remove(p)
                    del p
            
            for item in self._items:
                item.update()

            for laser in self.lasers:
                laser.update()

       

    def _update_players(self):
        self._barres[0].update()


    def drawEntities(self):

        self._draw_tiles()

        self._draw_balls()

        self._draw_players()

        if self.allow_items:
            self._draw_items()

    def _draw_items(self):
        for item in self._items:
            item.draw()

        for laser in self.lasers:
            laser.draw()

    def _draw_particles(self):
        for particle in self._particle_array:
            particle.draw()
                
    
    def _draw_tiles(self):
        for tile in self._tiles:
            tile.draw()
        
    def _draw_balls(self):
        for ball in self._balls:
            ball.draw()

    def _draw_players(self):
        self._barres[0].draw()

            
        
    def hasWon(self):
        return len(self._tiles) == 0
    
    def draw_text(
        self, text: str, position: Position, color: Color,
        show_rect: bool = False, rot: bool = False
    ) -> None:

        rect = pygame.Rect(position[0], position[1],
                           MIN_TEXT_RECT, TEXT_HEIGHT)
        text_surface = self.font.render(text, True, color)

        if rot:
            text_surface = pygame.transform.rotate(text_surface, rot)

        rect.w = min(MIN_TEXT_RECT, text_surface.get_width()+5)
        if show_rect:
            pygame.draw.rect(self.screen, (255, 255, 255), rect)
        self.screen.blit(text_surface, rect)


    def reset_keys(self) -> None:
        """Remettre l'état des touches à leur valeur de base"""
        for key in self._key_states.keys():
            self._key_states[key] = (False, None)
            
            
    def render(self):
        global GENERATION
        self.screen.fill((0,0,0))
        
        if self._status == SCENE_DEFEAT:
            self.loadLoseScreenAndExit()
        elif self._status == SCENE_VICTORY:
            self.loadWinScreenAndExit()
        else:
            
            self.drawEntities()
            self.draw_text(f"Gen:{GENERATION} - Score:{self.fitness}",(DISPLAY_WIDTH-200,0),(255,255,255))
            
        pygame.display.flip()




    