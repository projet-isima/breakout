import os
import pygame

from typing import Dict

from State import State
from constantes import *
from Button import Button



class Pause(State):

    def __init__(self, game) -> None:

        super().__init__(game)

        self.screen = pygame.display.get_surface()
        dimension = (MBUTTON_W, MBUTTON_H)

        self.reprendre_bouton = Button((MBUTTON_X, PLAY_Y2 + 10), "Bouton_reprendre.png", dimension)
        self.quitter_bouton = Button((MBUTTON_X, PLAY_Y4 - 10), "Bouton_6_Quitter.png", dimension)


    def _process_left_click(self, click : Position) -> None:
        
        if self.reprendre_bouton.rect.collidepoint(click):
            SELECT_SOUND.play()
            self.game.popStateStack()
            
            
        if self.quitter_bouton.rect.collidepoint(click):
            SELECT_SOUND.play()
            self.game.popStateStack()
            self.game.popStateStack()
            self.game.popStateStack()


    def update(self, key_state: Dict[str, KeyData]) -> None:

        if key_state["left_click"][0] == True:
            a = self._process_left_click(key_state["left_click"][1])


    def render(self) -> None:

        pygame.draw.rect(self.screen, GREY, pygame.Rect(177, 95, 251, 190), 15) 

        self.reprendre_bouton.render()
        self.quitter_bouton.render()

