import os
from typing import Dict, Union

import pygame

from constantes import *
from GamePhase import GamePhase
from State import State
from Item import Item
from ShopScene import ShopScene 
from AbstractLevelLoader import AbstractLevelLoader


class LevelLoader(AbstractLevelLoader):
    def __init__(self, game, levelname: str,shopAvailable=True) -> None:
        super().__init__(game,levelname,shopAvailable)
        self.perks.append(0)
        

    def _init_perks(self):
        return [0]
    
    def _load_next_game_scene(self):
        self.actualScene = GamePhase.loadSceneFrom(self.game,self.screen, os.path.join(
                "Levels", self.levelname, self.scenes[self.selected_index]),player_perk=self.perks)
        self.selected_index += 1
        self.perk  = self._init_perks()
   
    
    def _isGamePhase(self,scene):
        return isinstance(scene,GamePhase)

   
   