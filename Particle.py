import random
import math
import pygame
from constantes import *


class Particle:
    def __init__(self, maximumSize: int, x: float, y: float,color=(255,255,255)) -> None:

        self.screen = pygame.display.get_surface()
        self.displayWidth = DISPLAY_WIDTH
        self.displayHeight = DISPLAY_HEIGHT
        self.maximumSize = maximumSize
        self.x = x
        self.y = y
        self.size = 1 + int(random.random() * self.maximumSize)
        self.randomGen = random.Random()
        self.angle = self.randomGen.uniform(0, 2*math.pi)
        self.vitesse = self.randomGen.uniform(-2, 2)
        self.velx = self.vitesse * math.cos(self.angle)
        self.vely = self.vitesse * math.sin(self.angle)
        self.lifetime = 255
        self.color = color

    def update(self) -> None:
        self.px = self.x
        self.py = self.y

        self.x += self.velx

        self.y += self.vely
        self.lifetime -= 5

    def finished(self) -> bool:
        return self.lifetime <= 0

    def draw(self) -> None:
        if not(self.finished()):
            surf = pygame.Surface((self.size, self.size))
            surf.set_alpha(self.lifetime)

            pygame.draw.circle(surf, self.color, (0, 0), self.size)
            self.screen.blit(surf, (self.x, self.y))
