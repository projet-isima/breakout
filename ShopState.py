import pygame 
from constantes import *
from State import State
from Item import Item


class ShopState(State):
    def __init__(self,game,levelLoader):
        super().__init__(game)
        self.levelLoader = levelLoader
        self.choices = [Item(i,(i-1)*DISPLAY_WIDTH,DISPLAY_HEIGHT/2) for i in range(4)]
            
    def update(self): 
        pass
        
    def render(self):
        for choice in self.choices:
            choice.draw()

