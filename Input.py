import pygame
from constantes import *
import os
from typing import Dict


class Input:

    DARKISH_BLUE = (8, 30, 67)
    DARKISH_GREEN = (12, 51, 43)

    def __init__(self, x: int,  y: int) -> None:
        self._screen = pygame.display.get_surface()
        self._text = ""
        self._active = False
        self.initial_x = x
        self.x = x
        self.y = y
        self._input_rect = pygame.Rect(self.x, self.y, 50, 32)
        self._font = pygame.font.Font(os.path.join(
            "assets", "nk57_monospace", "nk57-monospace-cd-sb.ttf"), 20)

    def clear(self) -> None:
        self._text = ""

    def reactToClick(self, position: Position) -> None:
        if self._input_rect.collidepoint(*position):
            self.activate()
        else:
            self.deactivate()

    def activate(self) -> None:
        self._active = True

    def deactivate(self) -> None:
        self._active = False

    def react(self, keystate: Dict[str, KeyData]) -> None:
        if keystate["backspace"][0]:
            self.deleteLast()

        if keystate["characters"][0] and self._active:
            self.addToText(keystate["characters"][1])

        if keystate["left_click"][0]:
            self.reactToClick(keystate["left_click"][1])

    def addToText(self, char: str) -> None:
        self._text += char

    def deleteLast(self) -> None:
        self._text = self._text[:-1]

    def getText(self) -> str:
        return self._text

    def draw(self):
        color = self.DARKISH_BLUE if self._active else self.DARKISH_GREEN
        text_surface = self._font.render(self._text, True, WHITE)
        self._input_rect.w = max(MIN_INPUT_WIDTH, text_surface.get_width()+10)
        self.x = self.initial_x - \
            max(self._input_rect.w - MIN_INPUT_WIDTH, 0) // 2
        self._input_rect.x = self.x

        pygame.draw.rect(self._screen, color, self._input_rect)

        self._screen.blit(
            text_surface, (self._input_rect.x + 5, self._input_rect.y+5))
