import pygame


class Projectile(pygame.sprite.Sprite):
    def __init__(self, x: int, y: int, size: int) -> None:
        pygame.sprite.Sprite.__init__(self)

        self.size = size
        self.screen = pygame.display.get_surface()
        self.image = pygame.transform.scale(
            pygame.image.load(
                "assets/Laser/laser.png").convert().convert_alpha(),
            (self.size, 2 * self.size),
        )

        self.image.set_colorkey((255, 255, 255))
        self.mask = pygame.mask.from_surface(self.image)
        self.rect = self.image.get_rect()
        self.rect.left = x - self.size//2
        self.rect.bottom = y

        self.vely = -5

    def update(self, *args, **kwargs) -> None:
        if self.rect is not None:
            self.rect.y += self.vely

    def draw(self) -> None:
        if self.image and self.rect:
            self.screen.blit(self.image, self.rect)
