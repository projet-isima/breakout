import pygame
from State import State
import os
from constantes import *
from Button import Button
from SceneEditorState import SceneEditorState
from typing import Dict, Optional


class LevelGestionState(State):

    def __init__(self, game, levelName: str, new= False) -> None:
        super().__init__(game)

        self.screen = pygame.display.get_surface()

        self.levelName = levelName
        self.levelFolder = os.path.join("Levels", levelName)

        self.load_scenes()
        
        self.new = new 

        self.selected = None
        self.state = 0

        self.return_button = Button((0, DISPLAY_HEIGHT-30), "retour.png", (60, 30))
        self.addButton = Button(
            (DISPLAY_WIDTH - 70, DISPLAY_HEIGHT-65), "ajouter.png", (70, 30))

        self.supprimer_button = Button(
            (DISPLAY_WIDTH-70, DISPLAY_HEIGHT-30), "supprimer.png", (70, 30))
        self.edit_button = Button(
            (DISPLAY_WIDTH-70, DISPLAY_HEIGHT-30), "reediter.png", (70, 30))

        self.bouton1 = Button(
            (50, 50), "Bouton_Selection.png", (180, 25))
        self.bouton2 = Button(
            (50, 85), "Bouton_Selection.png", (180, 25))
        self.bouton3 = Button(
            (50, 120), "Bouton_Selection.png", (180, 25))
        self.bouton4 = Button(
            (50, 155), "Bouton_Selection.png", (180, 25))
        self.bouton5 = Button(
            (50, 190), "Bouton_Selection.png", (180, 25))
        self.bouton6 = Button(
            (50, 225), "Bouton_Selection.png", (180, 25))
        self.bouton7 = Button(
            (50, 260), "Bouton_Selection.png", (180, 25))
        self.bouton8 = Button(
            (50, 295), "Bouton_Selection.png", (180, 25))
        self.bouton9 = Button(
            (50, 330), "Bouton_Selection.png", (180, 25))

        self.last_click = 0
        self.delay = 500

    def load_scenes(self) -> None:
        self.scenes = sorted(os.listdir(self.levelFolder))

    def _get_selected_scene(self, click: Position) -> Optional[str]:
        selected = None
        x, y = click
        for i, scene in enumerate(self.scenes):
            offset = len(f"Scene{i}") * FONT_SIZE//2
            if (
                x >= DISPLAY_WIDTH // 2 - offset - 165
                and x <= DISPLAY_WIDTH // 2 + offset - 165
                and y >= i * TILE_HEIGHT + 50 + 10*i
                and y <= (i + 1) * TILE_HEIGHT + 50 + 10*i
            ):
                selected = scene
        return selected

    def _delete_selected(self, selected: str) -> None:
        self.last_click = pygame.time.get_ticks()
        index = self.scenes.index(selected)
        os.remove(os.path.join(self.levelFolder, selected))
        self.load_scenes()
        for i, scene in enumerate(self.scenes[index:]):
            new_index = index + i + 1
            os.rename(os.path.join(self.levelFolder, scene), os.path.join(
                self.levelFolder, f"{self.levelName}_{new_index}.pickle"))

    def _switch_state(self, click: Position) -> None:
        self.last_click = pygame.time.get_ticks()
        if self.state == 0 and self.supprimer_button.rect.collidepoint(*click):
            SELECT_SOUND.play()
            self.state = 1

        elif self.state == 1 and self.edit_button.rect.collidepoint(*click):
            SELECT_SOUND.play()
            self.state = 0

    def _process_button_input(self, click: Position) -> None:

        if self.return_button.rect.collidepoint(*click):
            SELECT_SOUND.play()
            self.game.popStateStack()
            if self.new:
                self.game.popStateStack()
            

        if self.addButton.rect.collidepoint(*click):
            SELECT_SOUND.play()
            levelEdit = SceneEditorState(
                self.game, self.levelName, index=len(self.scenes))
            levelEdit.enter_stack()

        if (pygame.time.get_ticks() - self.last_click > self.delay):
            self._switch_state(click)

    def _introduce_scene_editor(self, selected: str) -> None:
        SceneEditorState.loadSceneFrom(
            self.game, self.levelName, selected).enter_stack()

    def _can_click(self) -> bool:
        return pygame.time.get_ticks() - self.last_click > self.delay

    def _process_left_click(self, click):
        if selected := self._get_selected_scene(click):
            SELECT_SOUND.play()
            if self.state == 0:
                self._introduce_scene_editor(selected)

            elif (self._can_click()):
                self._delete_selected(selected)

        self._process_button_input(click)

    def update(self, key_state: Dict[str, KeyData]) -> None:

        self.load_scenes()

        if key_state["left_click"][0] == True:

            self._process_left_click(key_state["left_click"][1])

    def _render_buttons(self) -> None:
        if self.state == 0:
            self.supprimer_button.render()
        else:
            self.edit_button.render()
        self.addButton.render()
        self.return_button.render()

        self.bouton1.render()
        self.bouton2.render()
        self.bouton3.render()
        self.bouton4.render()
        self.bouton5.render()
        self.bouton6.render()
        self.bouton7.render()
        self.bouton8.render()
        self.bouton9.render()

    def _render_selection(self) -> None:
        
        """Affiche la sélection à l'écran"""
        for i in range(9):
            color = (0, 0, 0)
            mouse_pos = pygame.mouse.get_pos()

            if i < len(self.scenes):
                offset = len(f"Scene{i}")*FONT_SIZE//2
                if (
                    mouse_pos[0] >= DISPLAY_WIDTH // 2 - offset - 165
                    and mouse_pos[0] <= DISPLAY_WIDTH // 2 + offset - 165
                    and mouse_pos[1] >= i * TILE_HEIGHT + 50 + 10*i
                    and mouse_pos[1] <= (i + 1) * TILE_HEIGHT + 50 + 10*i
                ):
                    color = (255, 255, 255)
                self.game.draw_text(
                    f"Scene {i+1}", (DISPLAY_WIDTH//2-offset//2 - 165, i*TILE_HEIGHT + 50 + 10*i), color)
            else:
                offset = 3*FONT_SIZE//2
                self.game.draw_text(
                    "Vide", (DISPLAY_WIDTH//2-offset//2 - 165, i*TILE_HEIGHT + 50 + 10*i), color)

    def render(self) -> None:

        pygame.draw.rect(self.screen, GREY, pygame.Rect(30, 30, 225, 350), 10)

        self._render_buttons()
        
        self._render_selection()
        
