import os
from typing import Dict, Union

import pygame

from constantes import *
from GamePhase import GamePhase
from State import State
from Item import Item
from AbstractLevelLoader import AbstractLevelLoader
import pickle
from GamePhaseIA import GamePhaseIA

IA_FILE = 'IAModels/Items/ItemLord/ItemLord'

class LevelLoaderIA(AbstractLevelLoader):
    def __init__(self,game, levelname:str,shopAvailable=False):
        super().__init__(game, levelname,shopAvailable)
        with open(IA_FILE,"rb") as f:
            IA = pickle.load(f)
        
        self.net = neat.nn.FeedForwardNetwork.create(IA, CONFIG_ITEMS)
        
        
    def _init_perks(self):
        return [0]
    
    def _update_keystate(self,keystate):
        for key in keystate.keys():
            if key != "escape":
                keystate[key] = False,None
                
        if self.actualScene._has_started:
            output = self.net.activate(self.actualScene.return_scaled_inputs())
            
            if (output[0] >= output[1] and output[0] >= output[2]):
                keystate["right"] = True,None
            elif (output[1] >= output[2] and output[1] >= output[0]):
                keystate["left"] = True,None
        
        else:
            keystate["space"] = True, None
            
    def _load_next_game_scene(self):
        self.actualScene = GamePhaseIA.loadSceneFrom(self.game,self.screen, os.path.join(
            "Levels", self.levelname, self.scenes[self.selected_index]),self.perks)
        
        self.selected_index += 1
        self.perks = self._init_perks()
        return self.actualScene
        
    def _isGamePhase(self,scene):
        return isinstance(scene,GamePhaseIA)