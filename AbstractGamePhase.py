import itertools
import time
import math
import pygame
from Barre import Barre
from Balle import Balle
from Tile import Tile
from Pause import Pause
from constantes import *
import pickle
from Particle import Particle
from Item import Item
import random
from pygame import mixer
from abc import ABC, abstractmethod

class AbstractGamePhase(ABC):


    def __init__(self, game, tiles=None, lives = 3, allow_items=True,player_perk=None):
        super().__init__()
        self.game = game
        self.screen = pygame.display.get_surface()
        self.display_width, self.display_height = self.screen.get_size()

        self._tiles = pygame.sprite.Group()

        if tiles is None:
            for i in range(10):
                self._tiles.add(
                    Tile(self.screen, i*(TILE_WIDTH+GAP), 100, 1, TILE_WIDTH, TILE_HEIGHT))
        else:
            for i in range(len(tiles)):
                self._tiles.add(tiles[i])

        self._barres = []
        self._balls = []
        
        self._particle_array = [] 
        self._items = []
        
        self._init_entities(player_perk)
        self._has_started = False
        self._score = 0

        self._status = SCENE_ONGOING

        
        self.last_pause = 0

        self.lasers = pygame.sprite.Group()
        self.pause_delay = 500

        self.number_of_lives = lives
        self.number_of_stars_collected = 0
        
        self.allow_items = allow_items

        self.starRate = 0.5
        self.itemSpawnRate = 0.6
        self.particle_enable = True


    @abstractmethod
    def _init_entities(self,player_perk=None):
        """Initialise les entités de la classe"""
        ...
        
    def getStatus(self):
        return self._status
    
    def get_number_of_stars(self):
        return self.number_of_stars_collected
    
    def get_number_of_lives(self):
        return self.number_of_lives


    
    @classmethod
    @abstractmethod
    def constructFromTiles(cls, game, tiles,player_perk=None):
        """Retourne la scène de jeu construite à partir des tuile en paramètre"""
        ...
    
    @classmethod
    def getReprFromFile(cls,screen,filename):
        """Retourne les tuiles représentées dans filename"""
        repr_tile = []
        with open(filename, "rb") as f:
            repr_tile = pickle.load(f)
        tiles = [
            Tile(
                screen,
                j * (TILE_WIDTH + GAP),
                i * (TILE_HEIGHT + GAP),
                repr_tile[i][j],
                TILE_WIDTH,
                TILE_HEIGHT,
            )
            for i, j in itertools.product(
                range(len(repr_tile)), range(len(repr_tile[0]))
            )
            if repr_tile[i][j] != 0
        ]
        return tiles
        
    
    @classmethod
    def loadSceneFrom(cls, game,screen, filename,player_perk=None):
        """Charge la scène de jeu à partir du fichier représentatif 
        des tuiles"""
        tiles = cls.getReprFromFile(screen,filename)
        return cls.constructFromTiles(game,tiles,player_perk)


    @abstractmethod
    def _process_horizontal_movement(self, key_state):
        """Gère les mouvements horizontaux des joueurs"""
        ...
            
        
    def _prelaunch_on_space_pressed(self):
        """Action quand la barre espace est pressé pendant la phase de lancement"""
        self._has_started = True
        for barre in self._barres: 
            barre.inplay = True
        for ball in self._balls:
            ball.launch()


    
    def _process_pre_launch_ball_orientation(self, key_state):
        """S'occupe de la réponse à la barre espace pendant la phase de lancement"""
        if key_state["space"][0] == True:
            self._prelaunch_on_space_pressed()
            
    
    def _remove_offscreen_balls(self):
        for ball in self._balls[::-1]:
            if ball.isOffScreen():
                self._balls.remove(ball)
                del ball
    
    def _process_spawn_items(self,hit):
        if self.allow_items and (random.random() < self.itemSpawnRate):
            self.spawnItemAtLocation(*hit.getPosition())

        
    @abstractmethod
    def _manage_ball_hit(self,hit):
        """S'occupe des actions quand une tuile est touchée"""
        ...
        
    
    def _process_particles(self,hit,ball):
    
        color = (255,255,255)
        if (isinstance(hit, Tile)):

            if (hit.getType() == 1):
                color = BLUE
            elif (hit.getType() == 2):
                color = GREEN
            elif (hit.getType() == 3):
                color = PURPLE
            else:
                color = RED

        self.spawnParticlesAt(ball.get_pos(),color)

    def _process_ball_logic_in_game(self):
        
        for ball in self._balls:
            collision_happened, hit = ball.move()


            self._manage_ball_hit(hit)
                
            if collision_happened :
                HIT_SOUND.play()
                ball.up_speed()
                if self.particle_enable:
                    self._process_particles(hit,ball)
                    
                    
        self._remove_offscreen_balls()

    def _process_lasers_and_hits(self):
        for laser in iter(self.lasers):
            hit = pygame.sprite.spritecollideany(laser, self._tiles)

            if hit is not None:
                self.lasers.remove(laser)
                hit.life -= 1

               
                if hit.getLife() <= 0:
                    self._tiles.remove(hit)
                    self._score += hit.getType()
                    self._process_spawn_items(hit)

    def _activate_item(self,item,player):
        player.receiveSignal(item.getType())
        
        if (item.getType() ==5):
            COIN_SOUND.play()
            self.number_of_stars_collected += 1
        else:
            POWER_SOUND.play()
            if (item.getType() == 3):

                self.addABall()
                self.addABall()
                
        self._items.remove(item)
        
    def _check_item_collision(self, item):
        return next((barre for barre in self._barres if barre.isInCollisionWith(item)), None) 

        
    def _process_items(self):
        for i in range(len(self._items)-1, -1, -1):
            player = None
            if player := self._check_item_collision(self._items[i]):
                self._activate_item(self._items[i],player)
                break

    def _process_in_game(self):
        if self._has_started:

            self._process_ball_logic_in_game()
            
            if self.allow_items:
                self._process_lasers_and_hits()
                self._process_items()

        self.updateEntities()

    def _pause(self): 
        self.last_pause = pygame.time.get_ticks()
        Pause(self.game).enter_stack()

        
    def _update_status(self):
        if self.hasLost():
                self.number_of_lives -= 1
                self._items = [] 
                self.lasers = pygame.sprite.Group()
                
                if self.number_of_lives == 0:
                    self._status = SCENE_DEFEAT
                    
                else:
                    self._init_entities()
                    self._has_started = False
                    
        elif self.hasWon():
            self._status = SCENE_VICTORY
            
            
    def update(self, key_state):
        
        
            
            
        pygame.key.set_repeat(10,10)
        if (
            key_state["escape"][0] == True
            and pygame.time.get_ticks() - self.last_pause >= self.pause_delay
        ):
            self._pause()

        if self._status != SCENE_PAUSE:
            self._process_horizontal_movement(key_state)

            if not(self._has_started):
                self._process_pre_launch_ball_orientation(key_state)

            self._process_in_game()
            
        self._update_status()
        
    
    def addABall(self):
        ball = Balle(self._balls[0].rect.x, self._balls[0].rect.y,
                      self._barres[0], self._tiles, (0.1+random.random(),0.1+random.random()), True)
        self._balls.append(ball)

    def spawnItemAtLocation(self, x, y):
        if random.random() < self.starRate: 
            self._items.append(Item(5,x,y))
        else:
            type_of_item = random.randint(1, 4)
            self._items.append(Item(type_of_item, x, y))

    def spawnParticlesAt(self, pos: Position,color=(255,255,255)):
        for _ in range(math.floor(self._balls[0].getSpeed() * 5)):
            self._particle_array.append(Particle(2, *pos,color))

    def hasLost(self):
        return len(self._balls) == 0

    def loadLoseScreenAndExit(self):
        defImg = pygame.image.load("./assets/defaite.png")
        defImg = pygame.transform.scale(
            defImg, (DISPLAY_WIDTH, DISPLAY_HEIGHT))

        self.screen.blit(defImg, (0, 0))
        pygame.display.flip()
        time.sleep(5)

    def loadWinScreenAndExit(self):
        vicImg = pygame.image.load("./assets/victoire.png")
        vicImg = pygame.transform.scale(
            vicImg, (DISPLAY_WIDTH, DISPLAY_HEIGHT))
        self.screen.blit(vicImg, (0, 0))

        pygame.display.flip()
        time.sleep(5)

    def getScore(self):
        return self._score

    def updateEntities(self):

        for ball in self._balls:
            ball.update()

        self._update_players()

        for i in range(len(self._particle_array)-1, -1, -1):
            if (self._particle_array[i].finished()):
                p = self._particle_array[i]
                self._particle_array.remove(p)
                del p
                

        if self.allow_items:
            for i in range(len(self._items)-1, -1, -1):
                if (self._items[i].offScreen()):
                    p = self._items[i]
                    self._items.remove(p)
                    del p
            
            for item in self._items:
                item.update()

            for laser in self.lasers:
                laser.update()

        for particle in self._particle_array:
            particle.update()

    def _update_players(self):
        for barre in self._barres:
            barre.update()

    
        
    def drawEntities(self):

        self._draw_tiles()

        self._draw_balls()

        self._draw_players()

        self._draw_particles()

        if self.allow_items:
            self._draw_items()

    def _draw_items(self):
        for item in self._items:
            item.draw()

        for laser in self.lasers:
            laser.draw()

    def _draw_particles(self):
        for particle in self._particle_array:
            particle.draw()
                
    
    def _draw_tiles(self):
        for tile in self._tiles:
            tile.draw()
        
    def _draw_balls(self):
        for ball in self._balls:
            ball.draw()

    def _draw_players(self):
        for barre in self._barres:
            barre.draw()
        
    def hasWon(self):
        return len(self._tiles) == 0

    def render(self):
        if self.getStatus() == SCENE_DEFEAT:
            self.loadLoseScreenAndExit()
        elif self.getStatus() == SCENE_VICTORY:
            self.loadWinScreenAndExit()
        else:
            self.drawEntities()
        