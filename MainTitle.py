import os
import pygame
import random
from State import State
from typing import Dict
from constantes import *
from Button import Button
from Balle import Balle
from Particle import Particle
from LevelSelectionToPlayState import LevelSelectionToPlayState
from LevelSelectionToMultiPlayState import LevelSelectionToMultiPlayState
from LevelSelectionToPlayStateIA import LevelSelectionToPlayStateIA

from LevelSelectionToEditState import LevelSelectionToEditState
from LevelSelectionToPlayMultiIAState import LevelSelectionToPlayMultiIAState
from pygame import mixer

mixer.init()

class MainTitle(State):

    def __init__(self, game) -> None:

        super().__init__(game)
        
        
        
        mixer.music.load("Music/Luke-Bergs-Ascensionmp3.wav")
        mixer.music.play(-1)

        self.screen = pygame.display.get_surface()
        dimension = (MBUTTON_W, MBUTTON_H)

        self.playbutton = Button(
            (MBUTTON_X, PLAY_Y), "Bouton_1_Jouer.png", dimension)
        self.playbutton2 = Button(
            (MBUTTON_X, PLAY_Y2), "Bouton_2_Multijoueur.png", dimension)
        self.playbutton3 = Button(
            (MBUTTON_X, PLAY_Y3), "Bouton_3_IA.png", dimension)
        self.playbutton4 = Button(
            (MBUTTON_X, PLAY_Y4), "Bouton_4_MultiIA.png", dimension)
        self.lEdibutton = Button(
            (MBUTTON_X, LEDI_Y), "Bouton_5_Editeur.png", dimension)
        self.quitbutton = Button(
            (MBUTTON_X, QUIT_Y), "Bouton_6_Quitter.png", dimension)

        self.balle = []
        self._particle_array = []

        for _ in range(3):
            ball = Balle(random.randint(0, 605),
                         random.randint(0, 420), None, None)
            ball.main_menu_init()
            self.balle.append(ball)

    def _introduce_level_selection_to_play(self) -> None:
        LevelSelectionToPlayState(self.game).enter_stack()

    def _introduce_level_selection_to_multiplay(self) -> None:
        LevelSelectionToMultiPlayState(self.game).enter_stack()

    def _introduce_level_selection_to_edit(self) -> None:
        LevelSelectionToEditState(self.game).enter_stack()
    
    def _introduce_level_selection_to_play_IA(self):
        LevelSelectionToPlayStateIA(self.game).enter_stack()

    def _introduce_level_selection_to_play_multiIA(self):
        LevelSelectionToPlayMultiIAState(self.game).enter_stack()
        
    def _process_left_click(self, click):
        if self.quitbutton.rect.collidepoint(*click):
            self.game.popStateStack()

        if self.playbutton.rect.collidepoint(*click):
            SELECT_SOUND.play()
            self._introduce_level_selection_to_play()

        if self.playbutton2.rect.collidepoint(*click):
            SELECT_SOUND.play()
            self._introduce_level_selection_to_multiplay()
        

        if self.lEdibutton.rect.collidepoint(*click):
            SELECT_SOUND.play()
            self._introduce_level_selection_to_edit()
            
        if self.playbutton3.rect.collidepoint(*click):
            SELECT_SOUND.play()
            self._introduce_level_selection_to_play_IA()
            
        if self.playbutton4.rect.collidepoint(*click):
            SELECT_SOUND.play()
            self._introduce_level_selection_to_play_multiIA()
            

    def spawnParticlesAt(self, pos: Position):
        for _ in range(math.floor(4 * 10)):
            self._particle_array.append(Particle(2, *pos))

    def update(self, keystate: Dict[str, KeyData]) -> None:
        if keystate["quit"][0] == True:
            self.game.popStateStack()

        if keystate["left_click"][0] == True:
            self._process_left_click(keystate["left_click"][1])

        for balles in self.balle:
            collision = balles.main_menu()
            if collision == True:
                self.spawnParticlesAt(balles.get_pos())

        for i in range(len(self._particle_array)-1, -1, -1):
            if (self._particle_array[i].finished()):
                self._particle_array.remove(self._particle_array[i])

        for particle in self._particle_array:
            particle.update()
            
        


    def render(self) -> None:

        pygame.draw.rect(self.screen, GREY, pygame.Rect(177, 59, 251, 294), 15)

        self.lEdibutton.render()
        self.quitbutton.render()
        self.playbutton.render()
        self.playbutton2.render()
        self.playbutton3.render()
        self.playbutton4.render()

        for balles in self.balle:
            balles.draw()

        for particle in self._particle_array:
            particle.draw()