from State import State
import pygame
import os
from SceneEditorState import SceneEditorState
from Input import Input
from constantes import *
from Button import Button
from LevelGestionState import LevelGestionState
from typing import Dict


class LevelNamingState(State):

    def __init__(self, game) -> None:

        super().__init__(game)

        self._init_buttons()
        self.base_font = pygame.font.Font(None, FONT_SIZE)
        self.input = Input(DISPLAY_WIDTH // 2 - MIN_INPUT_WIDTH//2,
                           DISPLAY_HEIGHT // 2 - (FONT_SIZE + 2 * INPUT_RECT_PADDING_Y)//2)

    def _init_buttons(self) -> None:
        self.save_button = Button(
            (DISPLAY_WIDTH//2 - 50, 2*DISPLAY_HEIGHT//3), "saveButton.png")
        self.return_button = Button((0, DISPLAY_HEIGHT-30), "retour.png", (60, 30))

    def render(self) -> None:
        self.input.draw()
        self.save_button.render()
        self.return_button.render()
        
    

    def _process_button_input(self, click: Position) -> None:
        if self.save_button.rect.collidepoint(*click):
            
            level_name = self.input.getText()
            try:
                SELECT_SOUND.play()
                os.mkdir(os.path.join("Levels", level_name))
                LevelGestionState(self.game, level_name,new=True).enter_stack()
            except FileExistsError:
                pass
            finally:
                self.input.clear()

        if self.return_button.rect.collidepoint(*click):
            self.game.popStateStack()

    def update(self, keystate: Dict[str, KeyData]) -> None:
        pygame.key.set_repeat(200, 200)
        self.input.react(keystate)

        if keystate["quit"][0] == True:
            self.game.popStateStack()

        elif keystate["left_click"][0] == True:
            self._process_button_input(keystate["left_click"][1])
