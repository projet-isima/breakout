import pygame
import os

class Button:
    def __init__(self,pos,imageFile, dim=(100,40)):
        self.screen = pygame.display.get_surface()
        self.image =  pygame.transform.scale(pygame.image.load(os.path.join("assets",imageFile)).convert().convert_alpha(),dim)
        self.image.set_colorkey((0,0,0))
        self.rect = self.image.get_rect(topleft = pos)
    
        
    def render(self):
        self.screen.blit(self.image ,self.rect)
    