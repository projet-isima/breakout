import pygame
from constantes import *
import os
from typing import Union, Optional

class Item(pygame.sprite.Sprite):

    def __init__(self, type: int, x: int, y: int) -> None:
        super().__init__()
        self._type = type
        self._screen = pygame.display.get_surface()

        self.image = pygame.image.load(os.path.join(
            "assets", "Items", ITEM_SPRITES[self._type]))
        
        if type == 5:
            self.image = pygame.transform.scale(
            self.image, (TILE_WIDTH//2, TILE_WIDTH//2))
        else:
            self.image = pygame.transform.scale(
                self.image, (TILE_WIDTH, TILE_HEIGHT))

        self.rect = self.image.get_rect(topleft=(x, y))

    def offScreen(self) -> Optional[bool]:
        return self.rect.y >= self._screen.get_size()[1] if self.rect else False

    def get_rect_copy(self) -> Optional[pygame.rect.Rect]:
        return self.rect.copy() if self.rect else None

    def getType(self) -> int:
        return self._type

    def update(self, *args, **kwargs) -> None:
        if self.rect :
            self.rect.y += 1

    def draw(self) -> None:
        if self.image and self.rect :
            self._screen.blit(self.image, self.rect)

    def drawChoice(self) -> None:
        if self.image and self.rect :
            pygame.draw.rect(self._screen, GREY, pygame.Rect(self.rect.x-20, self.rect.y-30, TILE_WIDTH*2+40, TILE_HEIGHT*2+120), 10) 
            self.image = pygame.transform.scale(self.image, (TILE_WIDTH*2, TILE_HEIGHT*2))
            self._screen.blit(self.image, self.rect)