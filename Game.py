from typing import *
import os
import pygame
from MainTitle import MainTitle
from constantes import *
import pygame
from State import State


class Game:

    def __init__(self) -> None:

        pygame.init()
        pygame.mixer.init()

        # pile des états
        self._states_stack: List[State] = []

        # écran du jeu
        self._screen = pygame.display.set_mode((DISPLAY_WIDTH, DISPLAY_HEIGHT))
        self.background_color = (0,0,0) 

        self.img_file = os.path.join("assets", "Background","background.jpg")
        self.background_image = pygame.image.load(self.img_file)
        self.background = self.background_image.convert()

        # dictionnaire des commandes à chaque frame. Valeurs : (bool,meta) -> True si enclenché , False sinon -  meta can be either None ,the position of the event (click) or an unicode character(keydown)

        self._key_states: Dict[str, KeyData] = {"left": (False, None), "right": (False, None), "up": (False, None), "down": (False, None), "space": (False, None), "escape": (
            False, None), "backspace": (False, None), "quit": (False, None), "left_click": (False, None), "right_click": (False, None), "characters": (False, None), 
            "q": (False, None),  "d": (False, None),"z": (False, None), "s": (False, None)}

        self._clock = pygame.time.Clock()

        self.running = True


        self._load_assets()

        self._load_states()


    #def _load_background_image(self, filename : str) -> None:
        #background_img_file = os.path.join(
            #"assets", "Background", filename)
        #self.background_img = pygame.image.load(
            #background_img_file).convert_alpha()
        #self.background_img = pygame.transform.scale(
            #self.background_img, self._screen.get_size())
        
        
    def game_loop(self) -> None:
        """
            Boucle principale du jeu
        """
        while self.running:

            self._process()
            self.update()
            self.render()

            if len(self._states_stack) == 0:
                """S'il n'y a plus d'états dans la pile, fin du jeu"""
                self.running = False

    def _load_states(self) -> None:
        """Initialise la pile d'états"""
        mainTitle = MainTitle(self)
        mainTitle.enter_stack()

    def _load_assets(self) -> None:
        """
        Charge les assets
        """
        self.assets_dir = os.path.join("assets", "nk57_monospace")
        self.font = pygame.font.Font(os.path.join(
            self.assets_dir, "nk57-monospace-cd-sb.ttf"), FONT_SIZE)

    @staticmethod
    def loadAndScaleImg(
        image_filename: str, width: int,
        height: int
    ) -> pygame.surface.Surface:
        
        """Renvoi l'image sous forme d'une surface avec la hauteur et la 
        largeur spécifiée"""

        img_file = os.path.join("assets", image_filename)
        img = pygame.image.load(img_file).convert().convert_alpha()
        img = pygame.transform.scale(img, (width, height))
        img.set_colorkey((0, 0, 0))

        return img


    def _processMultipleKeyDown(self,event : pygame.event.Event) -> None:
        
        """
            Rempli le dictionnaire des inputs avec les actions de l'utilisateur.
            Supporte plusieurs inputs à la fois.
        """
        
        
        if event.type != pygame.KEYDOWN:
            return
        keys = pygame.key.get_pressed() 
        
        
        if event.unicode.isalnum(): 
            self._key_states["characters"] = (True, event.unicode)
        if keys[pygame.K_LEFT]:
            self._key_states["left"] = (True, None)
        if keys[pygame.K_RIGHT]:
            self._key_states["right"] = (True, None)
        if keys[pygame.K_UP]:
            self._key_states["up"] = (True, None)
        if keys[pygame.K_DOWN]:
            self._key_states["down"] = (True, None)
        if keys[pygame.K_SPACE]:
            self._key_states["space"] = (True, None)
        if keys[pygame.K_BACKSPACE]:
            self._key_states["backspace"] = (True, None)
        if keys[pygame.K_ESCAPE]: 
            self._key_states["escape"] = (True, None)
        if keys[pygame.K_q]:
            self._key_states["q"] = (True, None)
        if keys[pygame.K_d]: 
            self._key_states["d"] = (True, None)
        if keys[pygame.K_s]:
            self._key_states["s"] = (True, None)
        if keys[pygame.K_z]:
            self._key_states["z"] = (True, None)
            
               
    def _processMouseEvent(self , event : pygame.event.Event) -> None:
        
        """
            Gère les inputs de type clic de souris
        """
        if event.type == pygame.MOUSEBUTTONDOWN:
            mouseStates = pygame.mouse.get_pressed(num_buttons=3)

            if mouseStates[0]:
                self._key_states["left_click"] = (
                    True, pygame.mouse.get_pos())
            if mouseStates[2]:
                self._key_states["right_click"] = (
                    True, pygame.mouse.get_pos())
    
        
    def _process(self) -> None:
        """
            Gestion des évènements
        """

        for event in pygame.event.get():

            if event.type == pygame.QUIT:
                self._key_states["quit"] = (True, None)
            else:
                self._processMultipleKeyDown(event)
                self._processMouseEvent(event)
                

    def update(self) -> None:
        """
            Délégation de la mise à jour à l'état tête de pile
        """
        if self._key_states["quit"][0]:
            self.running = False
        else:
            self._states_stack[-1].update(self._key_states)
            self.reset_keys()

    def render(self) -> None:
        """
            Délégation de l'affichage à l'état tête de pile
        """
        self._screen.blit(self.background,(0,0))
        if len(self._states_stack) > 0:
            self._states_stack[-1].render()

        self._clock.tick(FPS)
        pygame.display.flip()

    def draw_text(
        self, text: str, position: Position, color: Color,
        show_rect: bool = False, rot: bool = False
    ) -> None:

        """
            Ecrit le texte spécifié à la position spécifiée dans la 
            bonne couleur. si show_rect est mis à vrai, le rectangle englobant
            est montré. Si rot est vrai, le texte est écrit dans l'axe des y.
        """
        rect = pygame.Rect(position[0], position[1],
                           MIN_TEXT_RECT, TEXT_HEIGHT)
        text_surface = self.font.render(text, True, color)

        if rot:
            text_surface = pygame.transform.rotate(text_surface, rot)

        rect.w = min(MIN_TEXT_RECT, text_surface.get_width()+5)
        if show_rect:
            pygame.draw.rect(self._screen, (255, 255, 255), rect)
        self._screen.blit(text_surface, rect)

    def reset_keys(self) -> None:
        """Remettre l'état des touches à leur valeur de base"""
        for key in self._key_states.keys():
            self._key_states[key] = (False, None)

    def popStateStack(self) -> None:
        """
            Retire l'état en sommet de la pile
        """
        self._states_stack.pop()

    def stack(self, state) -> None:
        """
            ajoute un état au sommet de la pile
        """
        self._states_stack.append(state)

    def blitOnScreen(self, surface: pygame.Surface, rect: pygame.Rect) -> None:
        """
            Affiche une surface à l'écran à la position spécifiée par rect
        """
        self._screen.blit(surface, rect)

    def get_column(self, x: int) -> int:
        """
            Retourne la colonne correspondante à l'abcisse spécifiée
        """
        return x // (TILE_WIDTH + GAP)

    def get_row(self, y: int) -> int:
        """
            Retourne la ligne correspondante à l'ordonnée spécifiée
        """
        return y // (TILE_HEIGHT + GAP)

    def get_coords(self, pos: Position) -> Position:
        
        """Retourne la colonne et la ligne de la position spécifiée"""
        return self.get_column(pos[0]), self.get_row(pos[1])


if __name__ == "__main__":

    g = Game()

    g.game_loop()
