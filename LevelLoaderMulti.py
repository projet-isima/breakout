import os
from typing import Dict, Union

import pygame

from constantes import *
from GamePhaseMulti import GamePhaseMulti
from AbstractLevelLoader import AbstractLevelLoader
from State import State
from Item import Item


class LevelLoaderMulti(AbstractLevelLoader):
    def __init__(self, game, levelname: str,shopAvailable=True) -> None:
        super().__init__(game,levelname, shopAvailable)
        self.levelname = levelname
        self.screen = pygame.display.get_surface()
    

    def _init_perks(self):
        return [0,0]
    
    def _load_next_game_scene(self):
        self.actualScene = GamePhaseMulti.loadSceneFrom(self.game,self.screen, os.path.join(
            "Levels", self.levelname, self.scenes[self.selected_index]),self.perks)
        self.selected_index += 1
        self.perks = self._init_perks()
        
        return self.actualScene
    
    
    def _isGamePhase(self, scene):
        return isinstance(scene,GamePhaseMulti)