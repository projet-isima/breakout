import os
import pygame
from State import State
from Tile import Tile
import pickle
from constantes import *
from Button import Button
from typing import List, Dict, Optional, Union


class SceneEditorState(State):
    MIN_INPUT_WIDTH = 50
    INPUT_RECT_PADDING_X = 5
    INPUT_RECT_PADDING_Y = 5

    def __init__(self, game, levelname: str, representativeArray: Optional[Grid] = None, index: int = 0) -> None:
        super().__init__(game)

        self.selected = 0
        self.screen = pygame.display.get_surface()

        self.tiles: List[List[Union[Tile, None]]] = []

        self.levelname = levelname
        self.scene_index = index + 1

        self.setup(representativeArray)

    def setup(self, representativeArray: Optional[Grid]) -> None:
        if representativeArray is None:
            self.initializeTilesAndRepr()
        else:
            self.representativeArray = representativeArray
            self.initializeTilesFromRepr(representativeArray)

        self.base_font = pygame.font.Font(None, FONT_SIZE)

        self.init_selections()
        self.init_buttons()

    def initializeTilesAndRepr(self) -> None:
        self.representativeArray = []
        self.tiles = []
        for _ in range(MAX_TILES_VERT):
            line: List[Union[Tile, None]] = []
            line2: List[int] = []
            for _ in range(MAX_TILES_HOR):
                line.append(None)
                line2.append(0)
            self.tiles.append(line)
            self.representativeArray.append(line2)

    def initializeTilesFromRepr(self, representativeArray: Grid) -> None:
        for i in range(len(representativeArray)):
            line: List[Union[Tile, None]] = []
            for j in range(len(representativeArray[0])):
                type_of_tile = representativeArray[i][j]
                if type_of_tile != 0:
                    line.append(self.createTileAtCoords(j, i, type_of_tile))
                else:
                    line.append(None)
            self.tiles.append(line)

    @classmethod
    def loadSceneFrom(cls, game, levelName: str, sceneFileName: str) -> "SceneEditorState":
        repr_tile = []
        filename = os.path.join("Levels", levelName, sceneFileName)
        with open(filename, "rb") as f:
            repr_tile = pickle.load(f)

        return SceneEditorState(game, levelName, repr_tile)

    def init_buttons(self) -> None:

        self.save_button = self.game.loadAndScaleImg("saveButton.png", 70, 30)

        # transfer to loadAndScale
        button_x = DISPLAY_WIDTH-70-GAP
        button_y = DISPLAY_HEIGHT-30-GAP

        self.save_button_rect = pygame.Rect(button_x, button_y - 40, 70, 30)
        self.return_button = Button((button_x, button_y), "retour.png", (70, 30))

    def init_selections(self) -> None:
        self.choices = [
            Tile(
                self.screen,
                (i - 1) * (TILE_WIDTH + GAP),
                DISPLAY_HEIGHT - TILE_HEIGHT - GAP,
                i,
                TILE_WIDTH,
                TILE_HEIGHT,
            )
            for i in range(1, NUMBER_OF_TYPES_OF_TILES + 1)
        ]

    def save(self, filename: str) -> None:
        if not(os.path.exists("Levels")):
            os.mkdir("Levels")
        extended_filename = os.path.join(
            "Levels", self.levelname, f"{filename}.pickle")
        with open(extended_filename, "wb") as f:
            pickle.dump(self.getReprTiles(), f)

        self.game.popStateStack()

    def getReprTiles(self) -> Grid:
        return self.representativeArray

    def createTileAtCoords(self, column: int, row: int, type: int) -> Tile:

        return Tile(self.screen, column * (TILE_WIDTH + GAP), row * (TILE_HEIGHT + GAP), type, TILE_WIDTH, TILE_HEIGHT)

    def isSquareEmpty(self, column: int, row: int) -> int:
        return self.getReprTiles()[row][column] == 0

    def isSquareAvailable(self, column: int, row: int) -> int:
        return row < MAX_TILES_VERT and self.isSquareEmpty(column, row)

    def isSquareOccupied(self, column: int, row: int) -> int:
        return row < MAX_TILES_VERT and not(self.isSquareEmpty(column, row))

    def _process_button_input(self, click: Position) -> None:
        if self.save_button_rect .collidepoint(*click):
            SELECT_SOUND.play()
            self.save(f"{self.levelname}_{self.scene_index}")

        if self.return_button.rect.collidepoint(*click):
            SELECT_SOUND.play()
            self.game.popStateStack()

    def _process_left_click(self, click: Position) -> None:

        column, row = self.game.get_coords(click)

        if row == TOTAL_ROW - 1:
            # Eventuellement faire une classe sélecteur
            if column < NUMBER_OF_TYPES_OF_TILES:
                self.selected = self.choices[column].getType()

        elif self.selected != 0:
            self.placeTile(click)

        self._process_button_input(click)

    def _process_quit(self):
        if len(os.listdir(os.path.join("Levels", self.levelname))) == 0:
            os.rmdir(os.path.join("Levels", self.levelname))
            self.game.popStateStack()

    def update(self, keystate: Dict[str, KeyData]) -> None:

        if keystate["quit"][0] == True:
            self._process_quit()

        elif keystate["left_click"][0] == True:
            self._process_left_click(keystate["left_click"][1])

        elif keystate["right_click"][0] == True:
            self.eraseTile(keystate["right_click"][1])

    def eraseTile(self, position: Position) -> None:
        column, row = self.game.get_coords(position)

        if self.isSquareOccupied(column, row):
            self.representativeArray[row][column] = 0
            self.tiles[row][column] = None

    def placeTile(self, position: Position) -> None:
        column, row = self.game.get_coords(position)

        if self.isSquareAvailable(column, row):
            self.tiles[row][column] = self.createTileAtCoords(
                column, row, self.selected)
            self.representativeArray[row][column] = self.selected

    def renderTiles(self) -> None:
        for line in self.tiles:
            for tile in line:
                if tile is not None:
                    tile.draw()

    def renderSelectionLine(self) -> None:
        for (i, choice) in enumerate(self.choices):
            if i + 1 == self.selected:
                choice.scaleY(1.2)

            else:
                choice.scaleY(1)

            if choice.rect is not None:

                choice.rect.y = DISPLAY_HEIGHT - GAP - choice.rect.h
                choice.draw()

    def render(self) -> None:
        self.renderTiles()
        self.renderSelectionLine()

        pygame.draw.line(self.screen, (255, 255, 255), (0, (MAX_TILES_VERT) * (
            TILE_HEIGHT + GAP)), (DISPLAY_WIDTH, (MAX_TILES_VERT) * (TILE_HEIGHT + GAP)))
        self.game.blitOnScreen(self.save_button, self.save_button_rect)
        self.return_button.render()
