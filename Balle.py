import pygame as pg
import random
from constantes import *
import os
from AbstractBall  import AbstractBall

class Balle(AbstractBall):

    def __init__(self, x : float, y : float, barre , tiles, orientation=(0, -1), launched : bool = False) -> None:
        super().__init__(x,y,tiles,orientation,launched)
        self._init_paddles(barre)
            
    def collision_barre(self):

        if not self.rect.colliderect(self.barre.rect):
            return False,None

        else:
            self._reflect_on_paddle(self.barre)
        
        
        return True,self.barre
               

    def _init_paddles(self,data):
        self.barre = data
    
